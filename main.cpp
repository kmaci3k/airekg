#include <QApplication>
#include "Views/airecgmain.h"
#include "Controllers/appcontroller.h"

#define DEVELOP

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AirEcgMain w;
    AppController *controller = new AppController();
    controller->BindView(&w);
    w.show();   
    return a.exec();
}
