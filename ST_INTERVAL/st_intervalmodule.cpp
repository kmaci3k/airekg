#include "st_intervalmodule.h"

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif


St_IntervalModule::St_IntervalModule() : ProcessingModule()
{
}

void St_IntervalModule::st_coordinates(EcgData *data)
{
    QList<double> *temp1 = new QList<double>();
    QList<double> *temp2 = new QList<double>();
    QList<int> *temp3 = new QList<int>();
    QList<int> *temp4 = new QList<int>();
    int st_length = (int)data->info->frequencyValue*0.06; //dlugosc odcinka ST 60ms wyrazona w ilosci probek
    int st_length2 = (int)data->info->frequencyValue*0.08; //dlugosc odcinka ST 80 ms wyrażona w ilości próbek

    for(int i=0; i<(data->waves->size()-1); i++)
    {
        temp1->append((double)((data->waves->at(i)->QRS_end)/(data->info->frequencyValue)));
        temp2->append((double)(((data->waves->at(i)->QRS_end)+st_length2)/(data->info->frequencyValue)));
        temp3->append((int)(data->waves->at(i)->QRS_end));
        temp4->append((int)((data->waves->at(i)->QRS_end)+st_length2));
    }
    data->STbegin_x = temp1;
    data->STend_x = temp2;
    data->STbegin_x_probki = temp3;
    data->STend_x_probki = temp4;
}

void St_IntervalModule::st_values(EcgData *data)
{
    QList<double> *temp5 = new QList<double>();
    QList<double> *temp6 = new QList<double>();

    for(int i=0; i<data->STbegin_x_probki->size(); i++)
    {
        int current_index = data->STbegin_x_probki->at(i);
        int current_index2 = data->STend_x_probki->at(i);
        temp5->append((double)(data->ecg_baselined_mv->at(current_index)));
        temp6->append((double)(data->ecg_baselined_mv->at(current_index2)));
    }
    data->STbegin_y = temp5;
    data->STend_y = temp6;
    data->ST_intervals_amount = data->STend_x_probki->size();

    QList<double> *temp7 = new QList<double>();
    for(int i=0; i<data->STbegin_y->size(); i++)
    {
        temp7->append((data->STend_y->at(i))-(data->STbegin_y->at(i))); // uniesienie

    }
    data->ST_angle = temp7;
}

void St_IntervalModule::baselined_to_mv(EcgData *data)
{
    QList<double> *temp8 = new QList<double>();

    for(int i; i<data->ecg_baselined->size(); i++)
    {
        temp8->append((double)(data->ecg_baselined->at(i)/204.8));
    }
    data->ecg_baselined_mv = temp8;

}

void St_IntervalModule::st_parameters(EcgData *data)
{
    int Increasing = 0;
    int Flat = 0;
    int Decreasing = 0;

    for(int i=0; i<data->ST_angle->size(); i++)
    {
        if (data->ST_angle->value(i)>0.01)
        Increasing++;
        else if (data->ST_angle->value(i)<-0.01)
        Decreasing++;
        else
        Flat++;
    }

    data->Increasing = Increasing;
    data->Decreasing = Decreasing;
    data->Flat = Flat;

    int Uplift = 0;
    int Downlift = 0;
    int Horizontal = 0;

    for(int i=0; i<data->STend_y->size(); i++)
    {
        if (data->STend_y->value(i)>0.01)
        Uplift++;
        else if (data->STend_y->value(i)<-0.01)
        Downlift++;
        else
        Horizontal++;
    }

    data->Uplift = Uplift;
    data->Downlift = Downlift;
    data->Horizontal = Horizontal;

}

void St_IntervalModule::st_angle(EcgData *data)
{
    QList<double> *temp9 = new QList<double>();


    for(int i; i<data->ST_angle->size(); i++)

    {
        double help_var = 0;
        help_var = ((data->ST_angle->at(i))*100)/8;
        temp9->append((double)(help_var));

    }
    data->ST_angle2 = temp9;
}

void St_IntervalModule::intervals_detection(EcgData *data)
{
    int uplifted_increasing = 0;
    int uplifted = 0;
    int lowered = 0;

    for (int i; i<data->STend_y->size(); i++)
    {
        if ((data->STend_y->value(i)>0.1) && (data->ST_angle->value(i)>0.01))
            uplifted_increasing++;

        if (data->STend_y->value(i)>0.2)
            uplifted++;

        if (data->STend_y->value(i)<-0.1)
            lowered++;
    }
    data->Uplifted_increasing = uplifted_increasing;
    data->Uplifted = uplifted;
    data->Lowered = lowered;
    data->Detected = uplifted_increasing + uplifted + lowered;
}


void St_IntervalModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "St_IntervalModule";
#endif

    baselined_to_mv(data);
    st_coordinates(data);
    st_values(data);
    st_parameters(data);
    st_angle(data);
    intervals_detection(data);

}
