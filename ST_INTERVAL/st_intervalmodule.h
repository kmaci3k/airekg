#ifndef ST_INTERVALMODULE_H
#define ST_INTERVALMODULE_H

#include "Common/processingmodule.h"
#include <QObject>

class St_IntervalModule : public ProcessingModule
{
public:
    St_IntervalModule();
    virtual void Process(EcgData *data);

    void st_coordinates(EcgData *data);
    void st_values(EcgData *data);
    void baselined_to_mv(EcgData *data);
    void st_parameters(EcgData *data);
    void st_angle(EcgData *data);
    void intervals_detection(EcgData *data);


};

#endif // ST_INTERVALMODULE_H
