#ifndef HRV_DFTMODULE_H
#define HRV_DFTMODULE_H

#include "Common/processingmodule.h"

class Hrv_DftModule : public ProcessingModule
{
public:
    Hrv_DftModule();
    virtual void Process(EcgData *data);
};

#endif // HRV_DFTMODULE_H
