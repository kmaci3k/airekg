#include "hrv_dftmodule.h"

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

Hrv_DftModule::Hrv_DftModule() : ProcessingModule()
{
}

void Hrv_DftModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "Hrv_DftModule";
#endif
}
