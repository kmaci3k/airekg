#include "r_peaksmodule.h"

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

R_PeaksModule::R_PeaksModule() : ProcessingModule()
{
}

void R_PeaksModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "R_PeaksModule";
#endif

    //get input QList<double> signal from EcgData object
    QList<double> *input = data->ecg_baselined;
    int inputSize = input->size();

    //Copy contents of input into VectorDouble
    VectorDouble inputVector(inputSize);
    for(int i=0;i<inputSize;i++) inputVector[i] = input->at(i);

    double sfreq = (double)data->info->frequencyValue;

    //Create RPeaks class object
    RPeaks rPeaks(inputVector,((sfreq > 0) ? sfreq : 360.0));

    //Engage detection method
    //HILBERT_R_PEAKS_DETECTOR or PAN_TOMPKINS_R_PEAKS_DETECTOR
    rPeaks.EngageRPeaksDetector(data->settings->RPeaksMode);

    //Get output vector
    VectorUnsignedInt outputVector = rPeaks.getRPeaksSamples();
    int outputSize = outputVector.size();

    //Copy output vector to QList<double>
    QList<unsigned int> *output = new QList<unsigned int>();
    output->reserve(outputSize);
    for(int i=0;i<outputSize;i++) output->append(outputVector[i]);

    //set output QList<unsigned int> signal in EcgData object
    data->r_peaks = output;
}
