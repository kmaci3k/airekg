clear variables;
c_dir = cd;
cd([c_dir '\cpp_r_peaks_0']);

files = {'100', '101', '102', '103', '104', '105', '106', '107', '108', '109', ...
         '111', '112', '113', '114', '115', '116', '117', '118', '119', ...
         '121', '122', '123', '124', ...
         '200', '201', '202', '203', '205', '207', '208', '209', '210', ...
         '212', '213', '214', '215', '217', '219', '220', '221', '222', ...
         '223', '228', '230', '231', '232', '233', '234'}; % count = 48

for i=1:length(files)
    rs = importdata([files{i} '_r_peaks_samples_1.txt']);
    cppRPeaksSamples_0{i} = (rs+1);
end
cd(c_dir);

cd([c_dir '\cpp_r_peaks_1']);
for i=1:length(files)
    rs = importdata([files{i} '_r_peaks_samples_1.txt']);
    cppRPeaksSamples_1{i} = (rs+1);
end
cd(c_dir);

cd([c_dir '\cpp_r_peaks_2']);
for i=1:length(files)
    rs = importdata([files{i} '_r_peaks_samples_1.txt']);
    cppRPeaksSamples_2{i} = (rs+1);
end
cd(c_dir);