%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load 'ecg_baselined';
% signal = a; clear a t;
% % signal = a(1:3600); clear a t;
% % signal = a(5000:6500); clear a t;
% fs = 360;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load 'case39';
% t1 = round(6.1*1e7/360);
% t2 = round(6.21*1e7/360);
% signal = sig39(t1:t2);
% fs = 360;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load 'case29';
t1 = round(1.48*1e7/360);
t2 = round(1.58*1e7/360);
signal = sig29(t1:t2);
fs = 360;