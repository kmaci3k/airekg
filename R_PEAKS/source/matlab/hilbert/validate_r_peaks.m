clear variables;

testcase = '101';

current_directory = cd;
% re = regexp(current_directory,'\','end');
% r_peaks_directory = current_directory(1:re(end));
cd([current_directory '\out_test']);

rPeaksSamplesH = importdata([testcase '_r_peaks_samples_1.txt']);
rPeaksSamplesH = rPeaksSamplesH+1;

rPeaksSamplesPT = importdata([testcase '_r_peaks_samples_2.txt']);
rPeaksSamplesPT = rPeaksSamplesPT+1;

ecgBaselined = importdata([testcase '_ecg_baselined.txt']);
ecgRaw = importdata([testcase '_MLII_values.txt']);

cd(current_directory);

% -----------------------------------------------

signal = ecgBaselined;
fs = 360;
dt = 1/fs;
time = 0:dt:dt*(length(signal)-1);

max_min = max(signal)-min(signal);

figure(2); hold off;

plot(time,signal); hold on;
for i=1:length(rPeaksSamplesH)
    plot([time(rPeaksSamplesH(i)) time(rPeaksSamplesH(i))],[0 signal(rPeaksSamplesH(i))],'--r');
    plot(time(rPeaksSamplesH(i)), signal(rPeaksSamplesH(i)),'or');
end
for i=1:length(rPeaksSamplesPT)
    plot([time(rPeaksSamplesPT(i)) time(rPeaksSamplesPT(i))],[0 signal(rPeaksSamplesPT(i))],'--m');
    plot(time(rPeaksSamplesPT(i)), signal(rPeaksSamplesPT(i)),'ms');
end
axis tight;
grid on;

allr = length(rPeaksSamplesH);
soon = 0;
late = 0;
for i=1:length(rPeaksSamplesH)
    if(signal(rPeaksSamplesH(i)-2) > signal(rPeaksSamplesH(i)))
        late = late + 1;
    elseif(signal(rPeaksSamplesH(i)+1) > signal(rPeaksSamplesH(i)))
        soon = soon + 1;
    end
end