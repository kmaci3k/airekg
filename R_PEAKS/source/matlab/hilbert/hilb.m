clear variables;
dt = 0.01;
t_end = 2.7;
t = 0:dt:t_end;
N = length(t);
fprintf('\n\nt_end: %d\n',t_end);
% x = zeros(size(t));
% for i=1:N
%     if(mod(ceil(t(i)),2) == 1)
%         x(i) = 0.5;
%     else
%         x(i) = -0.5;
%     end
% end

x = sin(5*t);
y = -cos(5*t);
hb = hilbert(x);
ihb = imag(hb);
figure(1); hold off;
plot(t,x,'g');
hold on;

plot(t,y,'k');
plot(t,ihb,'--r');

axis tight;
grid on;

window_length = 2.5; s_window_length = window_length/dt;    % adjust window_length so that s_window_length is an integer
window_overlap = 0.5; s_window_overlap = window_overlap/dt; % adjust window_overlap so that s_window_overlap is an integer
s_span = s_window_length - 2*s_window_overlap;
offset = s_window_length/2;                                 % adjust window_overlap so that s_window_overlap is divisable by 2
xlen = length(x);
x_m = []; ihb_m = [];
more = 1; % loop flag
iteration_no = 1;

% first iteration
if offset + s_window_length/2 >= xlen % last iteration
    xi = x((offset - s_window_length/2 + 1):xlen);
    fprintf('iteration %d, length(xi) = %d\n',iteration_no,length(xi));
    iteration_no = iteration_no + 1;
    hbi = hilbert(xi); ihbi = imag(hbi);
    x_m   = [x_m xi(1:end)];
    ihb_m = [ihb_m ihbi(1:end)];
    more = 0; % stop loop
else
    xi = x((offset - s_window_length/2 + 1):(offset + s_window_length/2));
    fprintf('iteration %d, length(xi) = %d\n',iteration_no,length(xi));
    iteration_no = iteration_no + 1;
    hbi = hilbert(xi); ihbi = imag(hbi);
    x_m   = [x_m xi(1 : end-s_window_overlap)];
    ihb_m = [ihb_m ihbi(1 : end-s_window_overlap)];
    offset = offset + s_span;
end
while more
    if offset + s_window_length/2 >= xlen % last iteration
        xi = x((offset - s_window_length/2 + 1):xlen);
        fprintf('iteration %d, length(xi) = %d\n',iteration_no,length(xi));
        iteration_no = iteration_no + 1;
        hbi = hilbert(xi); ihbi = imag(hbi);
        x_m   = [x_m xi(s_window_overlap+1:end)];
        ihb_m = [ihb_m ihbi(s_window_overlap+1:end)];
        more = 0; % stop loop
    else
        xi = x((offset - s_window_length/2 + 1):(offset + s_window_length/2));
        fprintf('iteration %d, length(xi) = %d\n',iteration_no,length(xi));
        iteration_no = iteration_no + 1;
        hbi = hilbert(xi); ihbi = imag(hbi);
        x_m = [x_m xi(s_window_overlap+1 : end-s_window_overlap)];
        ihb_m = [ihb_m ihbi(s_window_overlap+1 : end-s_window_overlap)];
        offset = offset + s_span;
    end
end
fprintf('Check: %d %d\n',sum(x == x_m) == length(x),length(ihb_m) == length(ihb));

plot(t, ihb_m, '--b');
legend('x = sin(5t)','x_H = -cos(5t)','x_H numerycznie (ca�y sygna�)',...
    'x_H numerycznie (fragmenty sygna�u)');
xlabel('t');
% ------ statistics --------

err = 0;
err_m = 0;
% integral square difference
dd = (y - ihb).^2;
dd_m = (y - ihb_m).^2;
for i=2:xlen
    err = err + dd(i) + dd(i-1);
    err_m = err_m + dd_m(i) + dd_m(i-1);
end
err = err*dt/2;
err_m = err_m*dt/2;
% err = trapz(dd)*dt;
% err_m = trapz(dd_m)*dt;
fprintf('err = %g\t err_m = %g\n',err,err_m);
fprintf('err_m/err = %g\n',err_m/err);