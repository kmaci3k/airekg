clear all;
load 'cpp_accuracy.mat';

N = length(refRPeaksTimes);
fs = 360;
dt = 1/fs;
meansuccess = [];
radiuses = [];

for radius = 0:19; % radius in samples

    radiuses = [radiuses radius];
    acc = zeros(N,5);

    for i=1:N
        calculated = cppRPeaksSamples_2{i}; % from cpp project (with baseline)

        reference = refRPeaksTimes{i};
        reference = round((reference*fs) + 1);

        clen = length(calculated);
        rlen = length(reference);

        detected = 0; % number of accurately detected R peaks in case i

        for j=1:rlen
            refsample = reference(j);
            flag = 0;
            for k=1:clen
                calsample = calculated(k);
                if(abs(refsample - calsample) <= radius)
                    flag = 1;
                    break;
                end
            end
            if(flag)
                detected = detected + 1;
            end
        end

        acc(i,1) = i;
        acc(i,2) = rlen;
        acc(i,3) = clen - rlen;
        acc(i,4) = 100*detected/rlen; % percentage of correct detections
        if(clen - rlen > 0) 
            acc(i,5) = 100*(clen - rlen)/rlen;
        end
    end
    ac = acc(:,4);
    meansuccess = [meansuccess mean(ac)];
end
radiuses = radiuses';
meansuccess = meansuccess'