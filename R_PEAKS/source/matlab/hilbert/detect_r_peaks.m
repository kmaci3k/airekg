% clear variables;
chooseinput;

dt = 1/fs;
time = 0:dt:dt*(length(signal)-1);

window_length = 200; % [s]
window_overlap = 30;

dsignal = zeros(length(signal),1);
for i=2:length(signal)
    dsignal(i) = (signal(i)-signal(i-1))/dt;
end
dtime = time(1:end);
% use windowed hilbert transform
hsignal = hilbert_w(signal, dt, window_length, window_overlap);
hdsignal = hilbert_w(dsignal, dt, window_length, window_overlap);

% use regular hilbert transform
% hsignal = imag(hilbert(signal));
% hdsignal = imag(hilbert(dsignal));

% plot(time,signal); grid; axis tight;
%%%%%%%%%%%%%%%%%%%%%
diffraw = hdsignal.^2 - dsignal.^2;
diffraw(:,2) = 1:size(diffraw,1);
diffs = sortrows(diffraw,1);
peaks(:,1) = diffs(end:-1:1,1);
peaks(:,2) = diffs(end:-1:1,2);
clear diffraw diffs;
r_peaks = get_r_peaks(signal, peaks,dt);
%%%%%%%%%%%%%%%%%%%%%
r_samples = r_peaks(:,2);
% r_samples = correct_r_peaks(r_peaks, signal);
%%%%%%%%%%%%%%%%%%%%%
max_min = max(signal)-min(signal);

figure(1); 
subplot(4,1,1); 
hold off;
plot(time,signal);
hold on;
for p_i=1:size(r_samples)
    plot([time(r_samples(p_i)) time(r_samples(p_i))],[0 signal(r_samples(p_i))],'--r');
    plot(time(r_samples(p_i)), signal(r_samples(p_i)),'or');
end
axis([time(1) time(end) min(signal)-0.1*max_min max(signal)+0.1*max_min]);
grid on;

% hold on;
subplot(4,1,2); hold off;
plot(dtime,dsignal,'g');
axis tight;
grid on;

subplot(4,1,3); hold off;
plot(time,sqrt(hdsignal.^2 + dsignal.^2),'r');
axis tight;
grid on;

subplot(4,1,4); hold off;
plot(dtime,hdsignal.^2 - dsignal.^2,'m');

axis tight;
grid on;

allr = length(r_samples);
soon = 0;
late = 0;
for i=1:length(r_samples)
    if(r_samples(i) > 1 && signal(r_samples(i)-1) > signal(r_samples(i)))
        late = late + 1;
    elseif(r_samples(i) < length(signal)-1 && signal(r_samples(i)+1) > signal(r_samples(i)))
        soon = soon + 1;
    end
end