function r_samples = correct_r_peaks(r_peaks, signal)
    r_samples = r_peaks(:,2);
    for i=1:length(r_samples)
        if(r_samples(i) > 1)
            if(signal(r_samples(i)-1) > signal(r_samples(i)))
                r_samples(i) = r_samples(i) - 1;
                continue;
            end
        end
        if(r_samples(i) < length(signal))
            if(signal(r_samples(i)+1) > signal(r_samples(i)))
                r_samples(i) = r_samples(i) + 1;
            end
        end
    end
end