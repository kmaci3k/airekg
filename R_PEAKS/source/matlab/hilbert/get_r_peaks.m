function res=get_r_peaks(signal, peaks,dt)
    threshold = peaks(1,1);
    cutoff = size(peaks,1);
    for i=1:size(peaks,1)
        if(peaks(i,1) < threshold/20)
            cutoff = i;
            break;
        end
    end
    peaks2(:,1) = peaks(1:cutoff-1,1);
    peaks2(:,2) = peaks(1:cutoff-1,2);
    
    max_bpm = 220;
    safety_coefficient = 0.8;
    max_bps = max_bpm/60;
    max_r_dt = 1/max_bps;
    % scope*dt < max_r_dt
    scope = floor(safety_coefficient*(max_r_dt/dt));

    for i=1:size(peaks2,1)
        if(peaks2(i,2) > 0)
            for j=i+1:size(peaks2,1)
                if(abs(peaks2(i,2)-peaks2(j,2)) <= scope)
                    peaks2(j,1) = 0;
                    peaks2(j,2) = 0;
                end
            end
        end
    end
    res(:,1) = peaks2(peaks2(:,2) > 0,1);
    res(:,2) = peaks2(peaks2(:,2) > 0,2);
    res = sortrows(res,2);
end