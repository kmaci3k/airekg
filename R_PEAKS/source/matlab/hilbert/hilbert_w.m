% get windowed hilbert transform
function res = hilbert_w(x, dt, window_length, window_overlap)
s_window_length = floor(window_length/dt);    % adjust window_length so that s_window_length is an integer
s_window_overlap = floor(window_overlap/dt); % adjust window_overlap so that s_window_overlap is an integer
if(mod(s_window_length,2) > 0)
    s_window_length = s_window_length-1;
end
s_span = s_window_length - 2*s_window_overlap;
offset = s_window_length/2;                                 % adjust window_overlap so that s_window_overlap is divisable by 2

xlen = length(x);
x_m = []; ihb_m = [];
more = 1; % loop flag
iteration_no = 1;

x = x';
% first iteration
if offset + s_window_length/2 >= xlen % last iteration
    xi = x((offset - s_window_length/2 + 1):xlen);
    iteration_no = iteration_no + 1;
    hbi = hilbert(xi); ihbi = imag(hbi);
    x_m   = [x_m xi(1:end)];
    ihb_m = [ihb_m ihbi(1:end)];
    more = 0; % stop loop
else
    xi = x((offset - s_window_length/2 + 1):(offset + s_window_length/2));
    iteration_no = iteration_no + 1;
    hbi = hilbert(xi); ihbi = imag(hbi);
    x_m   = [x_m xi(1 : end-s_window_overlap)];
    ihb_m = [ihb_m ihbi(1 : end-s_window_overlap)];
    offset = offset + s_span;
end
while more
    if offset + s_window_length/2 >= xlen % last iteration
        xi = x((offset - s_window_length/2 + 1):xlen);
        iteration_no = iteration_no + 1;
        hbi = hilbert(xi); ihbi = imag(hbi);
        x_m   = [x_m xi(s_window_overlap+1:end)];
        ihb_m = [ihb_m ihbi(s_window_overlap+1:end)];
        more = 0; % stop loop
    else
        xi = x((offset - s_window_length/2 + 1):(offset + s_window_length/2));
        iteration_no = iteration_no + 1;
        hbi = hilbert(xi); ihbi = imag(hbi);
        x_m = [x_m xi(s_window_overlap+1 : end-s_window_overlap)];
        ihb_m = [ihb_m ihbi(s_window_overlap+1 : end-s_window_overlap)];
        offset = offset + s_span;
    end
end
res = ihb_m';
end