clear variables;
close all;
clc;
load 'ref';

% refRPeaksTimes = {};
for fileindex=1:length(refSignal)
    fprintf(['fileindex = ' num2str(fileindex) ' \n']);
    signal = refSignal{fileindex};
    fs = 360;
    detect_r_peaks;
    RPeaksSamples{fileindex} = r_samples;
end