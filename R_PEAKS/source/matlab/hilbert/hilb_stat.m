% hilb_stat
stat = [];
test_vec = 1:1000;
for j=test_vec
    [a b] = hilb_f(j,0);
    stat = [stat b/a];
end
stat = sort(stat)';
fprintf('\n************************\nSuccess: %g %%\n',100*sum(stat <= 1)/length(test_vec));