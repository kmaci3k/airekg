#-------------------------------------------------
#
# Project created by QtCreator 2012-11-14T22:36:20
#
#-------------------------------------------------

QT       -= core gui

TARGET = R_PEAKS
TEMPLATE = lib

CONFIG += shared

DEFINES += R_PEAKS_EXPORTS

SOURCES += r_peaks.cpp

HEADERS += r_peaks.h

INCLUDEPATH += ../../Include

LIBS += -L ../../Libraries/ -llibfftw3-3
