﻿#include "r_peaks.h"
#define _USE_MATH_DEFINES
#include <math.h>

const double RPeaks::SAFETY_COEFFICIENT = 0.8;
const unsigned short RPeaks::MAX_BPM = 220;
const double RPeaks::THRESHOLD_DIVISOR = 20;
const unsigned int RPeaks::PT_LP_M = 9;
const unsigned int RPeaks::PT_HP_M = 56;
const unsigned int RPeaks::PT_MW_N = 58;


RPeaks::RPeaks(VectorDouble& signal, double samplingFreq)
{
	this->signal = signal;
	this->samplingFreq = samplingFreq;
}
VectorComplexDouble RPeaks::fft(VectorDouble& x)
{
	int N = x.size();
	fftw_complex *out = new fftw_complex[N];
	fftw_complex *in = new fftw_complex[N];
	fftw_plan plan_forward;

	for(int i=0;i<N;i++) 
	{
		in[i][0] = x[i];
		in[i][1] = 0.0;
	}
	plan_forward = fftw_plan_dft_1d(N,in,out,FFTW_FORWARD,FFTW_ESTIMATE);
	fftw_execute(plan_forward);
	VectorComplexDouble res(N);
	for(int i=0;i<N;i++) res[i] = ComplexDouble(out[i][0],out[i][1]);
	delete[] out;
	delete[] in;
	fftw_destroy_plan(plan_forward);
	return res;
}
VectorComplexDouble RPeaks::ifft(VectorComplexDouble& x)
{
	int N = x.size();
	fftw_plan plan_backward;
	fftw_complex *in = new fftw_complex[N];
	fftw_complex *out = new fftw_complex[N];
	for(int i=0;i<N;i++)
	{
		in[i][0] = x[i].real();
		in[i][1] = x[i].imag();
	}
	plan_backward = fftw_plan_dft_1d(N,in,out,FFTW_BACKWARD,FFTW_ESTIMATE);
	fftw_execute(plan_backward);

	VectorComplexDouble res(N);
	for(int i=0;i<N;i++) res[i] = ComplexDouble(out[i][0]/double(N),out[i][1]/double(N));

	delete[] in;
	delete[] out;
	fftw_destroy_plan(plan_backward);
	return res;
}
VectorDouble RPeaks::fftHilbert(VectorDouble& x)
{
	int N = x.size();
	VectorComplexDouble xdft = fft(x);
	VectorDouble h(N);

	if(N%2 == 0)
	{
		h[0] = 1;
		h[N/2] = 1;
		for(int i=1;i<N/2;i++) h[i] = 2;
	}
	else
	{
		h[0] = 1;
		for(int i=1;i<(N+1)/2;i++) h[i] = 2;
	}
	VectorComplexDouble xdfth(N);
	for(int i=0;i<N;i++) xdfth[i] = xdft[i]*h[i];

	VectorComplexDouble y = ifft(xdfth);
	VectorDouble yImag(N);
	for(int i=0;i<N;i++) yImag[i] = y[i].imag();

	return yImag;
}
VectorDouble RPeaks::fftHilbertWindowed(VectorDouble& x, double window_length, double window_overlap)
{
    unsigned int s_window_length = (unsigned int)(window_length*this->samplingFreq);
    unsigned int s_window_overlap = (unsigned int)(window_overlap*this->samplingFreq);
    if(s_window_length % 2)
        s_window_length--;

    unsigned int s_span = s_window_length - 2*s_window_overlap;
    unsigned int offset = s_window_length/2;

    unsigned int N = x.size();
    unsigned int Ni;
    VectorDouble ihb_m;
    VectorDouble xi;
    VectorDouble ihbi;
    bool more = true;

    //first iteration
    if(offset + s_window_length/2 - 1 >= N) //last iteration
    {
        xi.clear();
        for(unsigned int i=offset - s_window_length/2 ; i<N ; i++) xi.push_back(x[i]);
        Ni = xi.size();

        ihbi = fftHilbert(xi);
        for(unsigned int i=0;i<Ni;i++) ihb_m.push_back(ihbi[i]);
        more = false;
    }
    else
    {
        xi.clear();
        for(unsigned int i=offset - s_window_length/2 ; i<offset + s_window_length/2 ; i++) xi.push_back(x[i]);
        Ni = xi.size();

        ihbi = fftHilbert(xi);
        for(unsigned int i=0;i<Ni-s_window_overlap;i++) ihb_m.push_back(ihbi[i]);
        offset += s_span;
    }
    while(more)
    {
        if(offset + s_window_length/2 - 1 >= N) //last iteration
        {
            xi.clear();
            for(unsigned int i=offset - s_window_length/2 ; i<N ; i++) xi.push_back(x[i]);
            Ni = xi.size();

            ihbi = fftHilbert(xi);
            for(unsigned int i=s_window_overlap;i<Ni;i++) ihb_m.push_back(ihbi[i]);
            more = false;
        }
        else
        {
            xi.clear();
            for(unsigned int i=offset - s_window_length/2 ; i<offset + s_window_length/2 ; i++) xi.push_back(x[i]);
            Ni = xi.size();

            ihbi = fftHilbert(xi);
            for(unsigned int i=s_window_overlap;i<Ni-s_window_overlap;i++) ihb_m.push_back(ihbi[i]);
            offset += s_span;
        }
    }
    return ihb_m;
}
class DiffSorter
{
public:
	DiffSorter() {}
    bool operator()(const PairDoubleUnsignedInt& a, const PairDoubleUnsignedInt& b) const
	{
		return a.first > b.first;
	}
};
class PeaksCutSorter
{
private:
	int scope;
public:
	PeaksCutSorter(int scope) 
	{
		this->scope = scope;
	}
    bool operator()(const PairDoubleUnsignedInt& a, const PairDoubleUnsignedInt& b)
	{
        if(abs(a.second - b.second) <= this->scope) return false;
		else return a.second < b.second;
	}
};
VectorUnsignedInt RPeaks::getHilbertRPeaks(VectorPairDoubleUnsignedInt& peaks, double dt)
{
	double threshold = peaks[0].first;
	unsigned int cutoff = 0;
    while(cutoff < peaks.size() && peaks[cutoff].first >= threshold/(RPeaks::THRESHOLD_DIVISOR))
        cutoff++;
	
    VectorPairDoubleUnsignedInt peaks_cut(cutoff);
	for(unsigned int i=0;i<cutoff;i++) peaks_cut[i] = peaks[i];
	
    double max_bps = RPeaks::MAX_BPM/60;
	double max_r_interval = 1/max_bps;
    int max_scope = (int)floor(RPeaks::SAFETY_COEFFICIENT*(max_r_interval/dt));

	stable_sort(peaks_cut.begin(),peaks_cut.end(),PeaksCutSorter(max_scope));
	
    VectorUnsignedInt res_peaks;
    unsigned int fst = 0;
    unsigned int prevPeak;
    unsigned int possiblePeak;

    //first R peak
    if(this->signal.at(peaks_cut[fst].second) > 0)
    {
        res_peaks.push_back(peaks_cut[fst].second);
        prevPeak = peaks_cut[fst].second;
    }
    else
    {
        possiblePeak = peaks_cut[fst].second;
        fst++;
        while(fst<cutoff && (int)peaks_cut[fst].second - (int)possiblePeak <= max_scope)
        {
            if(this->signal.at(peaks_cut[fst].second) > 0)
            {
                possiblePeak = peaks_cut[fst].second;
                break;
            }
            fst++;
        }
        if(fst<cutoff) fst--;

        res_peaks.push_back(possiblePeak);
        prevPeak = possiblePeak;
    }
    //rest of R Peaks
    for(unsigned int i=fst+1;i<cutoff;i++)
    {
        if((int)peaks_cut[i].second - (int)prevPeak > max_scope)
        {
            if(this->signal.at(peaks_cut[i].second) > 0)
            {
                res_peaks.push_back(peaks_cut[i].second);
                prevPeak = peaks_cut[i].second;
            }
            else
            {
                possiblePeak = peaks_cut[i].second;
                i++;
                while(i<cutoff && (int)peaks_cut[i].second - (int)possiblePeak <= max_scope)
                {
                    if(this->signal.at(peaks_cut[i].second) > 0)
                    {
                        possiblePeak = peaks_cut[i].second;
                        break;
                    }
                    i++;
                }
                if(i<cutoff) i--;

                res_peaks.push_back(possiblePeak);
                prevPeak = possiblePeak;
            }
        }
    }
	return res_peaks;
}
void RPeaks::EngageHilbertRPeaksDetector()
{
	unsigned int N = this->signal.size();
	double dt = 1/this->samplingFreq;

	VectorDouble time(N);
	for(unsigned int i=0;i<N;i++) time[i] = i*dt;

	VectorDouble dsignal(N);
	for(unsigned int i=1;i<N;i++) dsignal[i] = (this->signal[i]-this->signal[i-1])/dt;

    VectorDouble hdsignal(N);
    //hdsignal = fftHilbert(dsignal);
    hdsignal = fftHilbertWindowed(dsignal, 200.0, 30.0);

    VectorPairDoubleUnsignedInt diff(N);
    for(unsigned int i=0;i<N;i++)
	{
		diff[i].first = hdsignal[i]*hdsignal[i] - dsignal[i]*dsignal[i];
		diff[i].second = i;
	}

	sort(diff.begin(),diff.end(),DiffSorter());
		
    this->rPeaksSamples = getHilbertRPeaks(diff,dt);
}
void RPeaks::EngageRPeaksDetector(unsigned char type)
{
    if(this->signal.size() == 0) return;

	switch (type)
	{
	case HILBERT_R_PEAKS_DETECTOR:
		this->EngageHilbertRPeaksDetector();
		break;
	case PAN_TOMPKINS_R_PEAKS_DETECTOR:
		this->EngagePanTompkinsRPeaksDetector();
		break;
	}
}
VectorUnsignedInt RPeaks::getRPeaksSamples(void)
{
	return this->rPeaksSamples;
}


//PAN TOMKINS
void RPeaks::EngagePanTompkinsRPeaksDetector()
{
    int N = this->signal.size();
    double dt = 1/this->samplingFreq;

    VectorDouble time(N);
    for(int i=0;i<N;i++) time[i] = i*dt;

    VectorDouble block_signal(N);
    double PtSign = 1.0, absMax = 0.0, indexMax;
    for (int i = 0 ; i < 2*samplingFreq; i++)
    {
         double temp = abs( (double) signal[i]) ;
         if (temp > absMax)
         {
             absMax = temp;
             indexMax = i;
         }
    }
    if (signal[indexMax] >= 0)
        PtSign = 1.0;
    else
        PtSign = -1.0;
    //

    for (int i=0; i<N; i++)
    {
        this->signal[i] = PtSign*this->signal[i];
        block_signal[i] = this->signal[i];
    }//abs((double) this->signal[i]);

    VectorDouble fsignal(N);
    VectorDouble mwisignal(N);

    for (int i=0; i<N; i++)
    {
        bool reset = false;
        if (i == 0) reset = true;

        block_signal[i] = PTLowPassFilter(block_signal[i], reset);
        block_signal[i] = PTHighPassFilter(block_signal[i],reset);
        fsignal[i] = block_signal[i];
        block_signal[i] = PTDerivative(block_signal[i], reset);
        block_signal[i] = PTSquaringFunction(block_signal[i]);
        block_signal[i] = PTMovingWindowIntegral(block_signal[i],reset);
        mwisignal[i] = block_signal[i];
    }

    //algorytm Pan Tompkins - wyszukiwanie peaków

    VectorPairDoubleUnsignedInt rPeaksBPF = PanTompkinsDo(fsignal);
    VectorPairDoubleUnsignedInt rPeaksMWI = PanTompkinsDo(mwisignal);
    //VectorPairDoubleUnsignedInt rPeaksBandPassFilter = rPeaksMovingWindowIntegration;
    //obliczenie delay:
    int max1 = Maximum(rPeaksBPF[1].second-50,rPeaksBPF[1].second+50,signal).second; //ok
    int dtFilter =rPeaksBPF[1].second-max1;
    if (dtFilter < 30)
        dtFilter = 35;
    int dtMWindow =rPeaksMWI[1].second-max1;

    for(int i=1; i<rPeaksBPF.size()-1; i++)
    {
        max1 = Maximum(rPeaksBPF[i].second-2*dtFilter,rPeaksBPF[i].second,signal).second;
        rPeaksBPF[i].second = max1;
        //rPeaksBandPassFilter[i].second -=dtFilter;
    }
    //for(int i=0; i<rPeaksMovingWindowIntegration.size(); i++) rPeaksMovingWindowIntegration[i].second -=dtMWindow;
    for(int i=1; i<rPeaksMWI.size()-1; i++)
    {
        max1 = Maximum(rPeaksMWI[i].second-PT_MW_N-dtFilter,rPeaksMWI[i].second,signal).second;
        rPeaksMWI[i].second = max1;
        //rPeaksMovingWindowIntegration[i].second -=dtMWindow;
    }

    this->rPeaksSamples = PanTompkinsGetPeaks(rPeaksBPF, rPeaksMWI);

    time.clear();
    fsignal.clear();
    mwisignal.clear();
    block_signal.clear();

}

//wyszukuje R_PEAKI
VectorPairDoubleUnsignedInt RPeaks::PanTompkinsDo(VectorDouble &signal)
{

    std::list<int> rr1, rr2;
    int RRAVERAGE1, RRAVERAGE2, RRLOWLIMIT, RRHIGHLIMIT, RRMISSEDLIMIT;
    //wartość oczekiwana signal peak, ogólnie peak, wartość oczekiwana noise peak, próg większy, próg mniejszy
    double SPK, PEAK, NPK, THRESHOLD1, THRESHOLD2;
    VectorDouble PTSignal, slopePTSignal, slopeRawSignal;

    bool mwindow = false;
    int N = this->signal.size();
    PTSignal.clear();
    slopePTSignal.clear();
    slopeRawSignal.clear();

    PTSignal.push_back(signal[0]);
    for (int i = 1; i < N; i++)
    {
        slopePTSignal.push_back(signal[i]-signal[i-1]);
        slopeRawSignal.push_back(this->signal[i]-this->signal[i-1]);
    }

    //INICJALIZACJA -----------------
    std::vector<PairDoubleUnsignedInt> qrsPeaks, noisePeaks;
    rr1.clear();
    rr2.clear();
    for (int i = 0; i < 8; i++)
    {
        rr1.push_back((int) (this->samplingFreq)); //1000ms
        rr2.push_back((int) (this->samplingFreq)); //1000ms
        PairDoubleUnsignedInt tPeak(0,0);
        if (!mwindow)
            tPeak = Maximum(i*this->samplingFreq+1,(i+1)*samplingFreq, signal);
        else
            tPeak = Maximum(i*this->samplingFreq+1,(i+1)*samplingFreq, slopePTSignal);
        qrsPeaks.push_back(tPeak);
    }
    int listMean = 0;
    for (std::list<int>::iterator it=rr1.begin() ; it != rr1.end(); it++)
        listMean+= (*it);
    listMean /= 8;
    RRAVERAGE1 = listMean;
    RRAVERAGE2 = RRAVERAGE1;
    RRLOWLIMIT = (int) (0.92*RRAVERAGE2);
    RRHIGHLIMIT = (int) (1.16*RRAVERAGE2);
    RRMISSEDLIMIT = (int) (1.66*RRAVERAGE2);

    PEAK = 0;
    for (int i = 0; i < 8; i++)
    {
        PEAK += qrsPeaks[i].first;
    }
    PEAK *= 0.125;
    SPK = PEAK;//maksymalna Mean(0,N,signal);
    NPK = 0;	//przykladowo
    THRESHOLD1 = NPK+0.25*(SPK-NPK);
    THRESHOLD2 = 0.5*THRESHOLD1;

    int index = 0, interval;
    double threshold = THRESHOLD1;
    bool qrs = false;
    PairDoubleUnsignedInt rpCandidate(signal[0],0), rpSearchBack (0,0), rpLast(signal[0],0);

    qrsPeaks.clear();
    while (index < N-1)
    {

        if (!mwindow)
            rpCandidate = Peak200MS(index,signal);
        else
            rpCandidate = Peak200MS(index,slopePTSignal);

        if (QRSCheck(rpCandidate,rpLast,threshold,slopePTSignal) != 0)
        //if (rpCandidate.first > threshold)
        {
            qrs = true;
            interval = (rpCandidate.second-rpLast.second)/samplingFreq;
            qrsPeaks.push_back(rpCandidate);
            rpLast = rpCandidate;
            double qrsMean = 0.0; int licz = 0;
            for (int i = qrsPeaks.size()-1; i > 0 && i > qrsPeaks.size()-9  ; i--)
            {
                qrsMean += qrsPeaks[i].first;
                licz++;
            }
            if (licz > 0) SPK = qrsMean/licz;
            if (licz == 8) SPK = 0.125*rpCandidate.first+0.875*SPK;
            THRESHOLD1 = NPK+0.25*(SPK-NPK);
            THRESHOLD2 = 0.5*THRESHOLD1;

            //indexSearchBack = index;
            rpSearchBack = PairDoubleUnsignedInt (0,index);
        }
        else
        {
            noisePeaks.push_back(rpCandidate);
            rpLast = rpCandidate;
            double noiseMean = 0.0; int licz2 = 0;
            for (int i = noisePeaks.size()-1; i > 0 && i > noisePeaks.size()-9  ; i--)
            {
                noiseMean += noisePeaks[i].first;
                licz2++;
            }
            if (licz2 > 0) NPK = noiseMean/licz2;
            if (licz2 == 8) NPK = 0.125*rpCandidate.first+0.875*NPK;
            THRESHOLD1 = NPK+0.25*(SPK-NPK);
            THRESHOLD2 = 0.5*THRESHOLD1;

            if (rpCandidate.first > rpSearchBack.first)
                rpSearchBack = rpCandidate;
            if (index - rpSearchBack.second > RRMISSEDLIMIT)
            {
                if (rpSearchBack.first > THRESHOLD2)
                {
                    qrs = true;
                    interval = (rpCandidate.second-rpLast.second)/samplingFreq;
                    qrsPeaks.push_back(rpCandidate);
                    rpLast = rpCandidate;
                    double qrsMean = 0.0; int licz = 0;
                    for (int i = qrsPeaks.size()-1; i > 0 && i > qrsPeaks.size()-9  ; i--)
                    {
                        qrsMean += qrsPeaks[i].first;
                        licz++;
                    }
                    if (licz > 0) SPK = qrsMean/licz;
                    if (licz == 8) SPK = 0.25*rpCandidate.first+0.75*SPK;
                    THRESHOLD1 = NPK+0.25*(SPK-NPK);
                    THRESHOLD2 = 0.5*THRESHOLD1;

                    rpSearchBack = PairDoubleUnsignedInt (0,index);
                }
            }
        }

        if (qrs)
        {
            qrs = false;
            //poprawienie czasow
            rr1.pop_front();
            rr1.push_back(interval);

            listMean = 0;
            for (std::list<int>::iterator it=rr1.begin() ; it != rr1.end(); it++) listMean+= (*it);
            listMean /= 8;
            RRAVERAGE1 = listMean;
            if (interval > RRLOWLIMIT && interval << RRHIGHLIMIT) //RRAVERAGE2
            {
                rr2.pop_front();
                rr2.push_back(interval);
                listMean = 0;
                for (std::list<int>::iterator it=rr2.begin() ; it != rr2.end(); it++) listMean+= (*it);
                listMean /= 8;
                RRAVERAGE2 = listMean;
                RRLOWLIMIT = (int) (0.92*RRAVERAGE2);
                RRHIGHLIMIT = (int) (1.16*RRAVERAGE2);
                RRMISSEDLIMIT = (int) (1.66*RRAVERAGE2);

            }
            threshold = THRESHOLD1;
            /*regularny, nieregularny rytm*/
            if (RRAVERAGE1 == RRAVERAGE2)
            {
                //isRegularHeartRate = true;
                threshold = THRESHOLD1;
                //thresh2 = THRESHOLD2;
            }
            else
            {
                //isRegularHeartRate = false;
                threshold = 0.5*THRESHOLD1;
                //thresh1 = 0.5*THRESHOLD1;
                //thresh2 = 0.5*THRESHOLD2;
            }

        }
        //threshold = THRESHOLD1;

    }
    return qrsPeaks;

}

PairDoubleUnsignedInt RPeaks::Peak200MS(int &index, VectorDouble &signal)
{
    int t200 = 0.2*samplingFreq;
    int preBlankCnt = 0;
    int indexPeak = 0;

    double aPeak, tempPeak, newPeak;
    tempPeak = 0.0;
    while (index < signal.size())
    {
        aPeak = signal[index];
        if (aPeak > 0 && !preBlankCnt)
        {
            tempPeak = aPeak;
            preBlankCnt = t200;
            indexPeak = index;
        }
        if (aPeak > tempPeak)
        {
            tempPeak = aPeak;
            preBlankCnt = t200;
            indexPeak = index;
        }
        else if (--preBlankCnt == 0)
        {
            newPeak = tempPeak;
            break;
        }

        index++;
    }
    if (index == signal.size())
    {
        newPeak = tempPeak;
        indexPeak = signal.size()-2;
    }
    else
        index = indexPeak+t200;
    return PairDoubleUnsignedInt(newPeak,indexPeak);
}


//0 - noise
//1 - qrs
int RPeaks::QRSCheck(PairDoubleUnsignedInt rpCandidate, PairDoubleUnsignedInt rpLast, double threshold, VectorDouble &slope)
{
    //regula3
    double intervalTime = (rpCandidate.second-rpLast.second)/samplingFreq;
    if (intervalTime < 0.36)
    {
        int beginLast=rpLast.second-0.36*samplingFreq; if(beginLast < 0) beginLast = 0;
        int beginCandidate = rpLast.second;//rpCandidate.second-0.36*samplingFreq;
        if (beginCandidate < 0 ) beginCandidate = 0;
        double slopeLast = Maximum(beginLast,rpLast.second, slope).first;
        double slopeCandidate = Maximum(beginCandidate,rpCandidate.second, slope).first;
        if (slopeCandidate*2 < slopeLast) return 0;
    }
    if (rpCandidate.first > threshold)
        return 1;
    else
        return 0;

}

//by comparison
//jeśli peak został wykryty dla obu przypadków to zostaje uznany
VectorUnsignedInt RPeaks::PanTompkinsGetPeaks(VectorPairDoubleUnsignedInt filter, VectorPairDoubleUnsignedInt movingWindow)
{
	VectorUnsignedInt res;
	
    int eps = 10;
	int i1 = 0, i2 = 0;
	int s1Size, s2Size;
	VectorPairDoubleUnsignedInt signal1, signal2;
	int temp1, temp2, temp21;

	if (filter.size() < movingWindow.size())
	{
		signal1 = filter;
		signal2 = movingWindow;
		
	}
	else
	{
		signal1 = movingWindow;
		signal2 = filter;
		
	}
	s1Size = signal1.size();
	s2Size = signal2.size();

	temp1 = signal1[i1].second;
	temp2 = signal2[i2].second;
	temp21 = signal2[i2+1].second;

    while (i1<s1Size)
    {
        temp1 = signal1[i1].second;
        while ((temp21 <= temp1+eps) && (i2 < s2Size-2))
        {
            i2++;
            temp21 = signal2[i2+1].second;
        }
        temp2 = signal2[i2].second;
        if (abs(temp1-temp2) <= eps)
        {
            res.push_back((temp1+temp2)/2);
        }

        i1++;
        while (i1 < s1Size-2 && signal1[i1].second < temp2)
        {
            i1++;
        }
    }

    temp1 = signal1[s1Size-1].second;
    temp2 = signal2[s2Size-1].second;
    if (abs(temp1-temp2) <= eps)
    {
        res.push_back((temp1+temp2)/2);
    }

	return res;
}
//funkcje uzyteczne
PairDoubleUnsignedInt RPeaks::Maximum(int begin, int end, VectorDouble &values)
{
    double maximum = values[begin];
    int location = begin;

    for (int i = begin; i < end; i++)
        if (values[i] > maximum)
        {
            maximum = values[i];
            location = i;
        }
    return PairDoubleUnsignedInt(maximum,location);
}
double RPeaks::Mean(int begin, int end, VectorDouble &values)
{
	double mean = 0;
	int width = end-begin;
	for (int i = begin; i < end; i++)
		mean+=values[i];
	mean /= width;
	return mean;
}
double RPeaks::PTLowPassFilter(double data, bool reset = false)
{
    static double y1 = 0, y2 = 0, x[(2*PT_LP_M+1)*2]; //(2m+1)*2
    static int n = 2*PT_LP_M;
    if (reset)
    {
        y1 = 0; y2 = 0; n = 2*PT_LP_M;
        for (int i = 0; i < (2*PT_LP_M+1)*2; i++)
            x[i] = 0;
    }

    double y0;
    x[n] = x[n + 2*PT_LP_M+1] = data;
    y0 = (2*y1) - y2 + x[n] - (2*x[n + PT_LP_M]) + x[n + 2*PT_LP_M];
    y2 = y1;
    y1 = y0;
    y0 /= (PT_LP_M*PT_LP_M);
    //y0 /= 32;
    if(--n < 0)
        n = 2*PT_LP_M;
    return(y0);
}
double RPeaks::PTHighPassFilter(double data, bool reset = false)
{
    static double y1 = 0, x[2*(PT_HP_M+1)];
    static int n = PT_HP_M;
    if (reset)
    {
        y1 = 0; n = PT_HP_M;
        for (int i=0;i<2*(PT_HP_M+1);i++) x[i] = 0;
    }

    double y0;
    x[n] = x[n + PT_HP_M+1] = data;
    y0 = y1 + x[n] - x[n + PT_HP_M];
    y1 = y0;
    if(--n < 0)
        n = PT_HP_M;
    return(x[n + (int) ceil((double) (PT_HP_M-1)/2)] - (y0/PT_HP_M));
}
double RPeaks::PTDerivative (double data, bool reset = false)
{
    double y;
    static double x_derv[6];
    if (reset)
    {
        for (int i=0; i<6; i++) x_derv[i]=0;
    }

    /*y = 1/8 (2x( nT) + x( nT - T) - x( nT - 3T) - 2x( nT -4T))*/
    y = (data*2) + x_derv[3] - x_derv[1] - ( x_derv[0]*2);
    y /= 8;
    //y = (data*3) + 2*x_derv[5] + x_derv[4] - x_derv[2] - (x_derv[1]*2)-( x_derv[0]*3);

    for (int i = 0; i < 3; i++)
        x_derv[i] = x_derv[i + 1];
    x_derv[3] = data;
    return y;
}
double RPeaks::PTSquaringFunction(double data)
{
    return data*data;
}
double RPeaks::PTMovingWindowIntegral(double data, bool reset = false)
{
      static double x[PT_MW_N], sum = 0;
      static int ptr = 0;
      if (reset)
      {
          ptr = 0;
          sum = 0;
          for (int i=0; i<PT_MW_N; i++) x[i]=0;
      }

      double y;
      sum -= x[ptr];
      sum += data;
      x[ptr] = data;
      if(++ptr == PT_MW_N)
            ptr = 0;

      y = sum/PT_MW_N;
      return y;
}
