#ifndef R_PEAKSMODULE_H
#define R_PEAKSMODULE_H

#include "Common/processingmodule.h"
#include <r_peaks.h>

class R_PeaksModule : public ProcessingModule
{
public:
    R_PeaksModule();
    virtual void Process(EcgData *data);
};

#endif // R_PEAKSMODULE_H
