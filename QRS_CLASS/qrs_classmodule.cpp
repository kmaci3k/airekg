#include "qrs_classmodule.h"
#include "qrsclass.h"

#include <iostream>

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

Qrs_ClassModule::Qrs_ClassModule() : ProcessingModule()
{
}

void Qrs_ClassModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "Qrs_ClassModule";
#endif

    if (!data || !data->ecg_baselined || data->ecg_baselined->isEmpty() || !data->waves || data->waves->isEmpty())
    {
#ifdef DEVELOP
        qDebug() << "Can't run QRS_CLASS_MODULE. Data is incomplite.";
#endif
        return;
    }

    QRSClassModule myClass;

    myClass.setSettings(data->settings->qrsClassSettings);

    myClass.setWaves(data->waves);
    myClass.setEGCBaseline(data->ecg_baselined);

    if (!myClass.process())
    {
        qDebug() << myClass.getErrorMessage();
    }
    else
    {
        QList<QRSClass>* classes = myClass.getClasses();
        data->classes = classes;
    }
}
