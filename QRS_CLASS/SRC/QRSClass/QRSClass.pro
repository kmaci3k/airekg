#-------------------------------------------------
#
# Project created by QtCreator 2013-01-08T00:25:27
#
#-------------------------------------------------

QT       -= gui

INCLUDEPATH += "C:/Users/Tomek/Downloads/tbb41_20121003oss_src2/tbb41_20121003oss/include/tbb"
INCLUDEPATH += "D:/AiR/Projekty/EKG/airekg/Include"

LIBS += -L"C:/Users/Tomek/Downloads/tbb41_20121003oss_src2/tbb41_20121003oss/build/windows_ia32_gcc_mingw4.4.0_release" \
             -ltbb \
             "D:/AiR/Projekty/EKG/airekg/Libraries/WAVES.dll"

TARGET = QRSClass
TEMPLATE = lib

DEFINES += QRSCLASS_LIBRARY

SOURCES += qrsclass.cpp \
    instance.cpp \
    Clusterers/abstractclusterer.cpp \
    Clusterers/kmeans.cpp \
    Clusterers/gmeans.cpp \
    Extractors/abstractextractor.cpp \
    Extractors/malinowskaextractor.cpp \
    Extractors/speedamplitudeextractor.cpp \
    Extractors/maxspeedexceedextractor.cpp

HEADERS += qrsclass.h\
    instance.h \
    Clusterers/abstractclusterer.h \
    Clusterers/kmeans.h \
    Clusterers/gmeans.h \
    Extractors/abstractextractor.h \
    Extractors/malinowskaextractor.h \
    Extractors/speedamplitudeextractor.h \
    Extractors/maxspeedexceedextractor.h

CONFIG += shared

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE7CC069C
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = QRSClass.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
