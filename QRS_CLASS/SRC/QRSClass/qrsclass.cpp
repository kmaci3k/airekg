#include "qrsclass.h"
#include <QDebug>
#include "instance.h"
#include "Extractors/abstractextractor.h"
#include "Extractors/malinowskaextractor.h"
#include "Extractors/speedamplitudeextractor.h"
#include "Extractors/maxspeedexceedextractor.h"
#include "Clusterers/abstractclusterer.h"
#include "Clusterers/kmeans.h"
#include "Clusterers/gmeans.h"
#include <tbb.h>

#include <QFile>
#include <iostream>


void QRSClassModule::setDefaultConfiguration()
{
    // TMP
    KMeans* clusterer = new KMeans();
    clusterer->setMaxIterations(1000);
    clusterer->setNumberOfClusters(4);
    this->clusterer = clusterer;
}


QRSClassModule::QRSClassModule()
{
    this->ecgBaselined = NULL;
    this->waves = NULL;
    this->extractors = new QList<AbstractExtractor*>;
    this->artifactsList = NULL;
    this->runParallel = false;
}

void QRSClassModule::setClusterer(ClustererType clustererType)
{
    switch (clustererType)
    {
    case KMeansClusterer:
    {
        KMeans* clusterer = new KMeans();
        clusterer->setMaxIterations(1000);
        clusterer->setNumberOfClusters(4);
        this->clusterer = clusterer;
        break;
    }
    default:
    {
        GMeans* clusterer = new GMeans();
        clusterer->setClusterNumbers(1,30);
        clusterer->setMaxIterations(10);
        this->clusterer = clusterer;
    }
    }
}

void QRSClassModule::setEGCBaseline(QList<double> *ecg)
{
    this->ecgBaselined = ecg;
}

void QRSClassModule::setWaves(QList<Waves::EcgFrame *> *waves)
{
    this->waves = waves;
}

bool QRSClassModule::setSettings(QRSClassSettings settings)
{
    switch(settings.clusterer)
    {
    case KMeansClusterer:
    {
        KMeans* clusterer = new KMeans();
        clusterer->setMaxIterations(settings.maxIterations);
        clusterer->setNumberOfClusters(settings.minClusterNo);
        this->clusterer = clusterer;
        this->runParallel = settings.parallelExecution;
        break;
    }
    default:
    {
        GMeans* clusterer = new GMeans();
        clusterer->setClusterNumbers(settings.minClusterNo,settings.maxClusterNo);
        clusterer->setMaxIterations(settings.maxIterations);
        this->runParallel = false;
        this->clusterer = clusterer;
    }
    }

    return true;
}

bool QRSClassModule::process()
{
    this->errMsg = "Everything Fine";
    if (this->ecgBaselined == NULL)
    {
        this->errMsg = "ECG SIGNAL NOT SET!";
        return false;
    }

    if (this->waves == NULL)
    {
        this->errMsg = "WAVES NOT SET!";
        return false;
    }

    if (this->clusterer == NULL)
    {
        this->errMsg = "CLUSTERER NOT SET!";
        return false;
    }

    if (this->artifactsList != NULL)
    {
        delete this->artifactsList;
    }
    this->artifactsList = new QList<int>();

    // Feature Extraction
    extractors->append(new MalinowskaExtractor());
    extractors->append(new SpeedAmplitudeExtractor());
    extractors->append(new MaxSpeedExceedExtractor());

//    QFile file("D:\\FeatureTest.m");
//    QTextStream stream(&file);
//    file.open(QIODevice::WriteOnly);

//    stream << "DATA = [";

    QList<Instance>* features = new QList<Instance>();
    for(int i = 0; i < waves->count(); i++)
    {
        QList<double> currentQRS;

        for(unsigned int j = waves->at(i)->QRS_onset; j <= waves->at(i)->QRS_end; j++)
        {
            currentQRS.append(this->ecgBaselined->at(j));
        }

        if (currentQRS.count() < 2)
        {
            this->artifactsList->append(i);
        }

        Instance currInstance(extractors->count());

        int j = 0;
        foreach(AbstractExtractor* extractor, *extractors)
        {
            currInstance[j] = extractor->extractFeature(currentQRS);
            j++;
        }

        features->append(currInstance);

    //    stream << "; \n";
    }

  //  stream << "];";
  //  stream.flush();
 //   file.close();

    // Cluster
    this->clusterer->setClusteringSet(features);

    if (this->runParallel)
    {
        if (!this->clusterer->classifyParallel())
        {
            this->errMsg = this->clusterer->getErrorMessage();
            return false;
        }
    }
    else
        if (!this->clusterer->classify())
        {
            this->errMsg = this->clusterer->getErrorMessage();
            return false;
        }
    return true;
}

QList<QRSClass> *QRSClassModule::getClasses()
{
    QList<QRSClass> *toReturn = new QList<QRSClass>();

    int classNo = 0;
    foreach(Instance currClass , *(this->clusterer->getClasses()))
    {
        QRSClass currentClass;
        currentClass.features = new QList<double>();
        currentClass.featureNames = new QList<QString>();
        currentClass.featureTooltip = new QList<QString>();
        currentClass.representative = new QVector<double>();
        currentClass.classMembers = new QList<int>();

        for(int i = 0 ; i < currClass.numberOfAttributes(); i++)
        {
            currentClass.features->append(currClass[i]);
            currentClass.featureNames->append(extractors->at(i)->getName());
            currentClass.featureTooltip->append(extractors->at(i)->getTooltip());
        }

        foreach(int sampleNo, *(this->clusterer->getClassMembers(classNo)))
        {
            if (!this->artifactsList->contains(sampleNo))
                currentClass.classMembers->append(sampleNo);
        }

        int representativeId = this->clusterer->getClassRepresentative(classNo);

        if (representativeId > -1)
        {
            int begin = waves->at(representativeId)->QRS_onset;
            int end = waves->at(representativeId)->QRS_end + 1;
            for(int j = begin; j < end; j++)
                currentClass.representative->append(ecgBaselined->at(j));
        }
        else
        {
            for(int j = 0; j < 30; j++)
                currentClass.representative->append(j);
        }
        currentClass.classLabel = QString("Class ").append(QString::number(classNo));

        if (currentClass.classMembers->count() > 0)
        {
            toReturn->append(currentClass);
        }

        classNo++;
    }

    QRSClass artifactClass;
    artifactClass.classLabel = QString("Artifacts");
    artifactClass.features = new QList<double>();
    artifactClass.featureNames = new QList<QString>();
    artifactClass.featureTooltip = new QList<QString>();
    artifactClass.representative = new QVector<double>();
    artifactClass.classMembers = new QList<int>();

    for(int i = 0 ; i < extractors->count(); i++)
    {
        artifactClass.features->append(0);
        artifactClass.featureNames->append(extractors->at(i)->getName());
        artifactClass.featureTooltip->append(extractors->at(i)->getTooltip());
    }

    artifactClass.representative->append(0);

    for(int i = 0 ; i < this->artifactsList->count(); i++)
    {
        artifactClass.classMembers->append(this->artifactsList->at(i));
    }

    QList<int> *artifacts = clusterer->getArtifacts();

    for(int i = 0; i < artifacts->count(); i++)
    {
        if (!artifactClass.classMembers->contains(artifacts->at(i)))
            artifactClass.classMembers->append(artifacts->at(i));
    }

    toReturn->append(artifactClass);

    return toReturn;
}

QString QRSClassModule::getErrorMessage()
{
    return this->errMsg;
}
