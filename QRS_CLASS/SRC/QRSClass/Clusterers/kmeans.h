#ifndef KMEANS_H
#define KMEANS_H

#include "Clusterers/abstractclusterer.h"
#include <tbb.h>
#include <QList>

class KMeansParallelHelper{

    QList<Instance> *block_instances;
    Instance* block_centroids;
    int numberOfClusters;
public:
    KMeansParallelHelper(QList<Instance> *input, Instance *inputCentroids, int numOfClusters);
    KMeansParallelHelper(KMeansParallelHelper &x, tbb::split);

    void operator()(const tbb::blocked_range<int> &r);
    void join(const KMeansParallelHelper& y);

    Instance* centroids;
    int* centroidsCount;
};


class KMeans : public AbstractClusterer
{
public:
    KMeans();
    ~KMeans();
    bool classify();
    bool classifyParallel();
    void setNumberOfClusters(int num);
    QList<Instance>* getClasses();
    QList<int>* getClassMembers(int classNumber);
    int getClassRepresentative(int classNumber);
    QList<Instance>* getCentroids();
    QList<Instance> getInstancesForCentroid(int i);
    int getNumberOfClusters();
};

#endif // KMEANS_H
