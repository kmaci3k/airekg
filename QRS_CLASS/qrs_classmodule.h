#ifndef QRS_CLASSMODULE_H
#define QRS_CLASSMODULE_H

#include "Common/processingmodule.h"

class Qrs_ClassModule : public ProcessingModule
{
public:
    Qrs_ClassModule();
    virtual void Process(EcgData *data);
};

#endif // QRS_CLASSMODULE_H
