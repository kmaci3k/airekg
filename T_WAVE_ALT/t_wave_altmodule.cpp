#include "t_wave_altmodule.h"
#include "T_WAVE_ALT.h"

#include <QFile>
#include <QTextStream>

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

T_Wave_AltModule::T_Wave_AltModule() : ProcessingModule()
{
}

void T_Wave_AltModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "T_Wave_AltModule";
#endif
    /*
        QFile filet;
        QTextStream t;
        QFile fileq;
        QTextStream q;
        QFile filebl;
        QTextStream bl;
        QFile filerp;
        QTextStream rp;
        filet.setFileName("t_end.txt");
        filet.open(QIODevice::WriteOnly | QIODevice::Text);
        t.setDevice( &filet );
        fileq.setFileName("qrs_end.txt");
        fileq.open(QIODevice::WriteOnly | QIODevice::Text);
        q.setDevice( &fileq );
        filebl.setFileName("baseline.txt");
        filebl.open(QIODevice::WriteOnly | QIODevice::Text);
        bl.setDevice( &filebl );
        filerp.setFileName("r_peaks.txt");
        filerp.open(QIODevice::WriteOnly | QIODevice::Text);
        rp.setDevice( &filerp );

        for(unsigned int i=0;i<data->ecg_baselined->length(); i++) {
            bl << data->ecg_baselined->at(i);
            bl << "\n";
        }
        for(unsigned int i=0;i<data->r_peaks->length();i++) {
            rp << data->r_peaks->at(i);
            rp << "\n";
        }
        for(unsigned int i=0;i<data->waves->length();i++) {
            q << data->waves->at(i)->QRS_end;
            q << "\n";
            t << data->waves->at(i)->T_end;
            t << "\n";
        }
        filet.close();
        fileq.close();
        filebl.close();
        filerp.close();
    */
    //----------------------------------------------------------------


    T_WAVE_ALT t_wave_alt;
    t_wave_alt.mode = data->TWA_mode;
    t_wave_alt.baselined = data->ecg_baselined;
    t_wave_alt.t_end = new QList<unsigned int>;
    t_wave_alt.qrs_end = new QList<unsigned int>;

    for(unsigned int i=0; i<data->waves->length(); i++) {
        t_wave_alt.t_end->append(data->waves->at(i)->T_end);
        t_wave_alt.qrs_end->append(data->waves->at(i)->QRS_end);
    }

    t_wave_alt.Process();
    t_wave_alt.t_end->clear();
    t_wave_alt.qrs_end->clear();

    data->TWA_positive_value = t_wave_alt.TWA_positive_value;
    data->TWA_positive = t_wave_alt.TWA_positive;
    data->TWA_negative_value  = t_wave_alt.TWA_negative_value;
    data->TWA_negative  = t_wave_alt.TWA_negative;
    data->twa_highest_val = t_wave_alt.twa_highest_val;
    data->twa_num = t_wave_alt.twa_num;

}
