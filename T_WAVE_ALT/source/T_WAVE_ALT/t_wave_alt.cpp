#include "t_wave_alt.h"
#include <QMap>
#include <QTextStream>
#include <cmath>
#include <QFile>
#include <qDebug>

void T_WAVE_ALT::Process()
{
    QList<double> odd_t_peaks;
    QList<double> even_t_peaks;
    QList<double> mma_even;
    QList<double> mma_odd;
    window_size = 128;
    int windowsNum;
    if (t_end->length()%window_size) {
        windowsNum = t_end->length()/window_size + 1;
    } else {
        windowsNum = t_end->length()/window_size;
    }

    TWA_positive_value = new QList<double>;
    TWA_positive = new QList<unsigned int>;
    TWA_negative = new QList<unsigned int>;
    TWA_negative_value = new QList<double>;
    t_wave_temp = new QList<Extremum>;
    twa_highest_val = new double;
    *(twa_highest_val) = 0.0;
    twa_num = new unsigned int;
    *(twa_num) = 0;
    if (mode!=1) {
        for(int window = 0; window < windowsNum; window++) {
            int currentWindowSize;
            if (t_end->length()%window_size) {
                currentWindowSize = window!=windowsNum-1 ? window_size : t_end->length()%window_size;
            } else {
                currentWindowSize = window_size;
            }
            if (currentWindowSize>1) {
                for(unsigned int t=0;t<currentWindowSize;t++) {
                    double mean;
                    Extremum extremum;
                    if (t_end->at(window*window_size+t)>baselined->length()-1 || qrs_end->at(window*window_size+t)>baselined->length()-1) {
                        extremum.value = 0;
                        extremum.index = 0;
                    } else {
                        mean = (baselined->at(t_end->at(window*window_size+t))+baselined->at(qrs_end->at(window*window_size+t)))/2;

                        Extremum maximum = countExtremum(baselined, t_end->at(window*window_size+t), qrs_end->at(window*window_size+t), true);
                        Extremum minimum = countExtremum(baselined, t_end->at(window*window_size+t), qrs_end->at(window*window_size+t), false);
                        if (abs(abs(minimum.value-mean)-abs(maximum.value-mean))> 30) {
                            if (abs(minimum.value-mean) < abs(maximum.value-mean)) {
                                extremum = maximum;
                            } else {
                                extremum = minimum;
                            }
                        } else {
                            if (t_end->at(window*window_size+t)-maximum.index < t_end->at(window*window_size+t)-minimum.index) {
                                extremum = maximum;
                            } else {
                                extremum = minimum;
                            }
                        }
                        extremum.value = extremum.value - mean;
                    }
                    if(t%2) {
                        even_t_peaks.append(extremum.value);
                    } else {
                        odd_t_peaks.append(extremum.value);
                    }
                    t_wave_temp->append(extremum);
                }
                mma_even.append(even_t_peaks.at(0));
                for (int i=1;i<even_t_peaks.length();i++) {
                    double delta;
                    double eta = (even_t_peaks.at(i-1)-mma_even.at(i-1))/8;
                    eta = eta * 5000 / 1024;
                    if (eta<=-32)
                        delta = -32;
                    else if (eta<=-1)
                        delta = eta;
                    else if (eta<0)
                        delta = -1;
                    else if (eta==0)
                        delta = 0;
                    else if (eta<=1)
                        delta = 1;
                    else if (eta<=32)
                        delta = eta;
                    else
                        delta = 32;

                    mma_even.append(mma_even.at(i-1)+delta);
                }
                mma_odd.append(odd_t_peaks.at(0));
                for (int i=1;i<odd_t_peaks.length();i++) {
                    double delta;
                    double eta = (odd_t_peaks.at(i-1)-mma_odd.at(i-1))/8;
                    eta = eta * 5000 / 1024;
                    if (eta<=-32)
                        delta = -32;
                    else if (eta<=-1)
                        delta = eta;
                    else if (eta<0)
                        delta = -1;
                    else if (eta==0)
                        delta = 0;
                    else if (eta<=1)
                        delta = 1;
                    else if (eta<=32)
                        delta = eta;
                    else
                        delta = 32;

                    mma_odd.append(mma_odd.at(i-1)+delta);
                }
                Extremum oddMax = countExtremum(&mma_odd,mma_odd.length(),0,true);
                Extremum evenMax = countExtremum(&mma_even,mma_even.length(),0,true);
                double diff = (evenMax.value-oddMax.value)/8;
                diff = diff<0 ? -diff : diff;

                if (diff > 1.9) {
                    for(unsigned int i=0; i<t_wave_temp->length();i++) {
                        TWA_positive->append(t_wave_temp->at(i).index);
                        TWA_positive_value->append(t_wave_temp->at(i).value);
                    }
                    *(twa_num) = *(twa_num) + 1;
                } else {
                    for(unsigned int i=0; i<t_wave_temp->length();i++) {
                        TWA_negative->append(t_wave_temp->at(i).index);
                        TWA_negative_value->append(t_wave_temp->at(i).value);
                    }
                }
                if (diff > *(twa_highest_val)) {
                    *(twa_highest_val) = diff;
                }
            }
            t_wave_temp->clear();
            mma_odd.clear();
            mma_even.clear();
            odd_t_peaks.clear();
            even_t_peaks.clear();
        }
    } else {
        for(unsigned int window = 0; window < t_end->length()-127; window++) {
            int currentWindowSize=128;
                for(unsigned int t=0;t<currentWindowSize;t++) {
                    double mean;
                    Extremum extremum;

                    mean = (baselined->at(t_end->at(window+t))+baselined->at(qrs_end->at(window+t)))/2;

                    Extremum maximum = countExtremum(baselined, t_end->at(window+t), qrs_end->at(window+t), true);
                    Extremum minimum = countExtremum(baselined, t_end->at(window+t), qrs_end->at(window+t), false);
                    if (abs(abs(minimum.value-mean)-abs(maximum.value-mean))> 30) {
                        if (abs(minimum.value-mean) < abs(maximum.value-mean)) {
                            extremum = maximum;
                        } else {
                            extremum = minimum;
                        }
                    } else {
                        if (t_end->at(window+t)-maximum.index < t_end->at(window+t)-minimum.index) {
                                extremum = maximum;
                        } else {
                                extremum = minimum;
                        }
                    }
                    extremum.value = extremum.value - mean;

                    if(t%2) {
                        even_t_peaks.append(extremum.value);
                    } else {
                        odd_t_peaks.append(extremum.value);
                    }
                    if(t==0) {
                        t_wave_temp->append(extremum);
                    }
                    else if (window==t_end->length()-128) {
                        t_wave_temp->append(extremum);
                    }
                }
                mma_even.append(even_t_peaks.at(0));
                for (int i=1;i<even_t_peaks.length();i++) {
                    double delta;
                    double eta = (even_t_peaks.at(i-1)-mma_even.at(i-1))/8;
                    eta = eta * 5000 / 1024;
                    if (eta<=-32)
                        delta = -32;
                    else if (eta<=-1)
                        delta = eta;
                    else if (eta<0)
                        delta = -1;
                    else if (eta==0)
                        delta = 0;
                    else if (eta<=1)
                        delta = 1;
                    else if (eta<=32)
                        delta = eta;
                    else
                        delta = 32;

                    mma_even.append(mma_even.at(i-1)+delta);
                }
                mma_odd.append(odd_t_peaks.at(0));
                for (int i=1;i<odd_t_peaks.length();i++) {
                    double delta;
                    double eta = (odd_t_peaks.at(i-1)-mma_odd.at(i-1))/8;
                    eta = eta * 5000 / 1024;
                    if (eta<=-32)
                        delta = -32;
                    else if (eta<=-1)
                        delta = eta;
                    else if (eta<0)
                        delta = -1;
                    else if (eta==0)
                        delta = 0;
                    else if (eta<=1)
                        delta = 1;
                    else if (eta<=32)
                        delta = eta;
                    else
                        delta = 32;

                    mma_odd.append(mma_odd.at(i-1)+delta);
                }
                Extremum oddMax = countExtremum(&mma_odd,mma_odd.length(),0,true);
                Extremum evenMax = countExtremum(&mma_even,mma_even.length(),0,true);
                double diff = (evenMax.value-oddMax.value)/8;
                diff = diff<0 ? -diff : diff;

                if (diff > 1.9) {
                    for(unsigned int i=0; i<t_wave_temp->length();i++) {
                        TWA_positive->append(t_wave_temp->at(i).index);
                        TWA_positive_value->append(t_wave_temp->at(i).value);
                    }
                    *(twa_num) = *(twa_num) + 1;
                } else {
                    for(unsigned int i=0; i<t_wave_temp->length();i++) {
                        TWA_negative->append(t_wave_temp->at(i).index);
                        TWA_negative_value->append(t_wave_temp->at(i).value);
                    }
                }
                if (diff > *(twa_highest_val)) {
                    *(twa_highest_val) = diff;
                }
                t_wave_temp->clear();
                mma_odd.clear();
                mma_even.clear();
                odd_t_peaks.clear();
                even_t_peaks.clear();
            }
    }
}


double T_WAVE_ALT::countMean(QList<double> *ecg_base, unsigned int value) {
    double mean=0;
    for(int i=0; i<20;i++) {
        mean += ecg_base->at(value-i);
    }
    return mean/20;
}

double T_WAVE_ALT::abs(double value) {
    return value>0 ? value : -value;
}

int T_WAVE_ALT::abs(int value) {
    return value>0 ? value : -value;
}

T_WAVE_ALT::Extremum T_WAVE_ALT::countExtremum(QList<double> *data, unsigned int endIndex, unsigned int beginingIndex, bool maximum) {
    Extremum result;
    result.index = beginingIndex;
    result.value = data->at(beginingIndex);

    if (maximum) {
        for(unsigned int i=beginingIndex;i<endIndex;i++) {
            if (result.value<data->at(i)) {
                 result.value=data->at(i);
                 result.index = i;
            }
        }
        return result;
    } else {
        for(unsigned int i=beginingIndex;i<endIndex;i++) {
            if (result.value>data->at(i)) {
                 result.value=data->at(i);
                 result.index = i;
            }
        }
        return result;
    }
}
