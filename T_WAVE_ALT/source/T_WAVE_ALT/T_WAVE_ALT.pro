#-------------------------------------------------
#
# Project created by QtCreator 2012-12-25T21:59:34
#
#-------------------------------------------------

QT       -= gui

TARGET = T_WAVE_ALT
TEMPLATE = lib

CONFIG += shared

DEFINES += T_WAVE_ALT_LIBRARY

SOURCES += t_wave_alt.cpp

HEADERS += t_wave_alt.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
