#ifndef T_WAVE_ALTMODULE_H
#define T_WAVE_ALTMODULE_H

#include "Common/processingmodule.h"

class T_Wave_AltModule : public ProcessingModule
{
public:
    T_Wave_AltModule();
    virtual void Process(EcgData *data);
};

#endif // T_WAVE_ALTMODULE_H
