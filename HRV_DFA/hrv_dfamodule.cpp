#include "hrv_dfamodule.h"
#include <fstream>
#include <algorithm>
#include <cmath>
#include "HRV_DFA.h"

//using namespace std;
#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

Hrv_DfaModule::Hrv_DfaModule() : ProcessingModule()
{
}

void Hrv_DfaModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "Hrv_DfaModule";
#endif

    // Zabezpieczenie przed wybuchem
    if(!data || !data->r_peaks || data->r_peaks->isEmpty() || data->r_peaks->size() == 0)
        return;

     HRV_DFA dfa_module;


    // Dane wejściowe
    dfa_module.sampleFreq = (double)data->info->frequencyValue;

    int rPeaksSize = data->r_peaks->size();
    for (int i=0; i<rPeaksSize; i++)
        dfa_module.r_peaks.append(data->r_peaks->at(i));


   dfa_module.Process();

   // Dane wyjsciowe
   int trend_x_size = dfa_module.trend_x.size();
   QList<double> *trendx = new QList<double>();
   trendx->reserve(trend_x_size);
   for (int i=0;i<trend_x_size;i++) trendx->append(dfa_module.trend_x.at(i));
   data->trend_x=trendx;

   int trend_y_size = dfa_module.trend_y.size();
   QList<double> *trendy = new QList<double>();
   trendy->reserve(trend_y_size);
   for (int i=0;i<trend_y_size;i++) trendy->append(dfa_module.trend_y.at(i));
   data->trend_y = trendy;

   int trend_z_size = dfa_module.trend_z.size();
   QList<double> *trendz = new QList<double>();
   trendz->reserve(trend_z_size);
   for (int i=0;i<trend_z_size;i++) trendz->append(dfa_module.trend_z.at(i));
   data->trend_z = trendz;

   int trend_v_size = dfa_module.trend_v.size();
   QList<double> *trendv = new QList<double>();
   trendv->reserve(trend_v_size);
   for (int i=0;i<trend_v_size;i++) trendv->append(dfa_module.trend_v.at(i));
   data->trend_v = trendv;

   int *window_min = new int;
   *window_min = dfa_module.window_min;
   data->window_min = window_min;

   int *window_max = new int;
   *window_max = dfa_module.window_max;
   data->window_max = window_max;

   int *window_plot = new int;
   *window_plot = dfa_module.window_plot;
   data->window_plot = window_plot;

   int *boxes = new int;
   *boxes = dfa_module.boxes;
   data->boxes = boxes;

   double *wsp_a = new double;
   *wsp_a = dfa_module.wsp_a;
   data->wsp_a = wsp_a;

   double *wsp_b = new double;
   *wsp_b = dfa_module.wsp_b;
   data->wsp_b = wsp_b;

   double *alfa = new double;
   *alfa = dfa_module.alfa;
   data->alfa = alfa;


}

