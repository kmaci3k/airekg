#ifndef HRV_DFAMODULE_H
#define HRV_DFAMODULE_H

#include "Common/processingmodule.h"

class Hrv_DfaModule : public ProcessingModule
{
public:
    Hrv_DfaModule();
    virtual void Process(EcgData *data);
};

#endif // HRV_DFAMODULE_H
