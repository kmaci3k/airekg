#include "hrv_dfa.h"
#include "cmath"
#include "algorithm"
#include "stdlib.h"

using namespace std;


void HRV_DFA::Process()
{
double m_dfa_sum1=0.0, m_dfa_sum2=0.0, m_dfa_sum3=0.0, m_dfa_sum4=0.0;
QList<double>  m_dfa_MainSum;
QList<double>  b_dfa_MainSum;
double  m_dfa_fluct;
double  b_dfa_fluct;
QList<double> trend;
QList<double> fluct_final;
QList<double> rIntervalsInt;

double rIntervalsSum=0.0;
double rIntervalsAvg=0.0;
double rIntervalsInt_init=0.0;
double fluct=0.0;
int window_counter;
int window;
int c=0;



//Zapobiegniecie braku pik�w
int rPeaksSize = this->r_peaks.size();
if (rPeaksSize == 0)
 return;

//Wyliczenie interwalow i przeskalownie do ms
QList<double> r_intervals;
rPeaksSize = this->r_peaks.size();
for (int i=0; i<rPeaksSize-1; i++)
r_intervals.append((this->r_peaks.at(i+1) - this->r_peaks.at(i))*1000.0 / (this->sampleFreq));
int rIntervalsSize = r_intervals.size();

//wyliczenie sredniego interwalu
for (int i=0; i<rPeaksSize-1; i++) {
rIntervalsSum+=r_intervals[i]; }
rIntervalsAvg=rIntervalsSum/rIntervalsSize;

//calkujemy sygnal
rIntervalsInt.append(r_intervals[0]-rIntervalsAvg);
for (int i=1; i<=rIntervalsSize-1; i++) {
rIntervalsInt_init=r_intervals[i]-rIntervalsAvg;
rIntervalsInt.append(rIntervalsInt[i-1]+ rIntervalsInt_init);
}
int rIntervalsIntSize = rIntervalsInt.size();

//Okna
window_min=50;
window_max=rIntervalsIntSize-1;
int window_size=10;
window_plot=100;

//Oknowanie
 for (int s=window_min;s<window_max-1;s=s+window_size) {

       m_dfa_MainSum.clear();
       b_dfa_MainSum.clear();
       trend.clear();

//Metoda najmniejszych kwadrat�w dla kazdego okna
for (int n=0; n<rIntervalsSize-1; n=n+s) {
for (int k=n; k<=min(n+s-1,rIntervalsIntSize-1);k++) {
    m_dfa_sum1=m_dfa_sum1+(k)*rIntervalsInt[k];
    m_dfa_sum2=m_dfa_sum2+k;
    m_dfa_sum3=m_dfa_sum3+rIntervalsInt[k];
    m_dfa_sum4=m_dfa_sum4+(k)*(k);
}
m_dfa_MainSum.append((((s)*m_dfa_sum1)-(m_dfa_sum2*m_dfa_sum3))/(((s)*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum2)));
b_dfa_MainSum.append(((m_dfa_sum3*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum1))/(((s)*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum2)));
m_dfa_sum1=0.0;
m_dfa_sum2=0.0;
m_dfa_sum3=0.0;
m_dfa_sum4=0.0;
}

//Wyznaczenie trendu dla kazdego okna
window_counter=0;
window=0;

for (int j=0;j<rIntervalsIntSize-1; j++) {
window_counter=window_counter+1;
if (window_counter<=s)
trend.append((j)*m_dfa_MainSum[window]+b_dfa_MainSum[window]);
else {
  window=window+1;
  trend.append((j)*m_dfa_MainSum[window]+b_dfa_MainSum[window]);
  window_counter=1;
}
}

//Usuniecie trendow lokalnych i wyliczenie fluktuacji
for (int i=0;i<rIntervalsIntSize-1;i++) {
    fluct = fluct + (rIntervalsInt[i]-trend[i])*(rIntervalsInt[i]-trend[i]);
    if (s==window_plot)
         this->trend_v.append((double)trend[i]);
}

fluct_final.append(sqrt((1/(double)(rIntervalsIntSize))*fluct));
c++;
fluct=0.0;
 }

 //Wyslanie danych do wykresu fluktuacji
 for (int i=0; i<c-1;i++) {
     this->trend_x.append((double)(log((double)(fluct_final[i]))));
     this->trend_y.append((double)(log((double)(window_min+i*window_size))));
}

 for (int i=0; i<c-1;i++) {
     m_dfa_sum1=m_dfa_sum1+(log((double)(window_min+i*window_size)))*log((double)(fluct_final[i]));
     m_dfa_sum2=m_dfa_sum2+log((double)(window_min+i*window_size));
     m_dfa_sum3=m_dfa_sum3+log((double)(fluct_final[i]));
     m_dfa_sum4=m_dfa_sum4+(log((double)(window_min+i*window_size)))*(log((double)(window_min+i*window_size)));
 }
 m_dfa_fluct=((((c-1)*m_dfa_sum1)-(m_dfa_sum2*m_dfa_sum3))/(((c-1)*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum2)));
 b_dfa_fluct=(((m_dfa_sum3*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum1))/(((c-1)*m_dfa_sum4)-(m_dfa_sum2*m_dfa_sum2)));
 m_dfa_sum1=0.0;
 m_dfa_sum2=0.0;
 m_dfa_sum3=0.0;
 m_dfa_sum4=0.0;




 //Wyslanie danych do wykresu trendu i scalkowanego sygnalu
for (int i=0; i<rIntervalsIntSize-1; i++)
{
    //this->trend_v.append((double)trend[i]);
    this->trend_z.append((double)rIntervalsInt[i]);
}

this->window_min=window_min;
this->window_max=window_max;
this->window_plot=window_plot;
this->boxes=c;
this->wsp_a=m_dfa_fluct;
this->wsp_b=b_dfa_fluct;
this->alfa=m_dfa_fluct;

}

