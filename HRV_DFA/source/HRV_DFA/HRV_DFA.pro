#-------------------------------------------------
#
# Project created by QtCreator 2013-01-08T11:03:17
#
#-------------------------------------------------

QT       -= gui

TARGET = HRV_DFA
TEMPLATE = lib
CONFIG += shared

DEFINES += HRV_DFA_LIBRARY

SOURCES += hrv_dfa.cpp

HEADERS += hrv_dfa.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xED8C082A
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = HRV_DFA.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
