#-------------------------------------------------
#
# Project created by QtCreator 2012-10-28T16:04:31
#
#-------------------------------------------------

include(Controllers/controllers.pri)
include(Common/common.pri)

include(ECG_BASELINE/ecg_baseline.pri)
include(R_PEAKS/r_peaks.pri)
include(WAVES/waves.pri)
include(HRV1/hrv1.pri)
include(HRV2/hrv2.pri)
include(HRV_DFA/hrv_dfa.pri)
include(QRS_CLASS/qrs_class.pri)
include(ST_INTERVAL/st_interval.pri)
include(T_WAVE_ALT/t_wave_alt.pri)
include(HRT/hrt.pri)
include(Views/views.pri)

PRECOMPILED_HEADER = precomp.h

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG(release, debug|release): DESTDIR = release/bin
CONFIG(debug, debug|release): DESTDIR = debug/bin

TARGET = ECG_APP
TEMPLATE = app

SOURCES += main.cpp

INCLUDEPATH += Include

LIBS += -L$$PWD/Libraries/ -lR_PEAKS
LIBS += -L$$PWD/Libraries/ -llibfftw3-3
LIBS += -lECG_BASELINE
LIBS += -L$$PWD/Libraries/ -lWAVES
LIBS += -L$$PWD/Libraries/ -lt_wave_alt
LIBS += -lQRSClass
LIBS += -lhrv2
LIBS += -lhrv_dfa

dlls.path = $$OUT_PWD/$$DESTDIR
dlls.files += $$files($$PWD/Libraries/*.dll)
INSTALLS += dlls

HEADERS += \
    precomp.h
