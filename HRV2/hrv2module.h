#ifndef HRV2MODULE_H
#define HRV2MODULE_H

#include "Common/processingmodule.h"

class Hrv2Module : public ProcessingModule
{
public:
    Hrv2Module();
    virtual void Process(EcgData *data);
};

#endif // HRV2MODULE_H
