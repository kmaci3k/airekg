#include "hrv2.h"
#include <QMap>
#include <QTextStream>
#include <cmath>
#include <QFile>

void HRV2::Process()
{
    // Tworzenie zmiennej interwalow miedzy zalamkami R (nieuzupelnionej)
    QList<double> r_intervals_nc;
    int rPeaksSize = this->rPeaks.size();
    for (int i=0; i<rPeaksSize-1; i++)
        r_intervals_nc.append((this->rPeaks.at(i+1) - this->rPeaks.at(i)) * 1000 / this->sampleFreq);
    int rIntervalsSize_nc = r_intervals_nc.size();

    // Tworzenie zmiennej interwalow miedzy zalamkami R (uzupelnionej)
    QList<double> r_intervals;
    int sum = 0;
    for (int i=0; i<rIntervalsSize_nc; i++)
        sum += r_intervals_nc[i];
    double mean = sum/rIntervalsSize_nc;
    for (int i=0; i<rIntervalsSize_nc; i++)
    {
        int wiel = DopasujWielokrotnoscSredniej(mean, r_intervals_nc[i]);
        if (wiel == 1)
        {
            r_intervals.append(r_intervals_nc[i]);
        }
        else
        {
            if (i+1 == rIntervalsSize_nc)
            {
                double avgInterval = r_intervals_nc[i]/wiel;
                for (int j=0; j<wiel; j++)
                    r_intervals.append(avgInterval);
            }
            else
            {
                int wiel2 = DopasujWielokrotnoscSredniej(mean, r_intervals_nc[i+1]);
                double avgInterval = (r_intervals_nc[i] + r_intervals_nc[i+1])/(wiel+wiel2);
                for (int j=0; j<(wiel+wiel2); j++)
                    r_intervals.append(avgInterval);
            }
        }
    }
    int rIntervalsSize = r_intervals.size();

    // Tworzenie histogramu
    double barWidth = 7.8125;
    QMap<unsigned int, int> histogram;
    for (int i=0; i<rIntervalsSize; i++)
        histogram[floor(r_intervals[i] / barWidth) * barWidth] = 0;

    for (int i=0; i<rIntervalsSize; i++)
        histogram[floor(r_intervals[i] / barWidth) * barWidth]++;
    int histogramSize = histogram.size();
    this->histogram_x = histogram.keys();
    this->histogram_y = histogram.values();

    // Obliczanie indeksu trojkatnego i wskaznika TINN
    int max = 0;
    for (int i=0; i<histogramSize; i++)
        if (max < this->histogram_y.at(i))
            max = this->histogram_y.at(i);
    this->triangularIndex = ((double)rIntervalsSize) / max;
    this->TINN = this->triangularIndex * barWidth;

    // Tworzenie danych wykresu Poincare
    for (int i=0; i<rIntervalsSize-1; i++)
    {
        this->poincare_x.append((unsigned int)r_intervals[i]);
        this->poincare_y.append((int)r_intervals[i+1]);
    }
    int poincareSize = this->poincare_x.size();

    // Obliczanie wskaźników SD1 i SD2
    sum = 0;
    for (int i=0; i<rIntervalsSize; i++)
        sum += r_intervals[i];
    mean = sum/rIntervalsSize;
    double SD1Temp = 0;
    double SD2Temp = 0;
    for (int i=0; i<poincareSize; i++)
    {
        SD1Temp += (this->poincare_x.at(i) - this->poincare_y.at(i)) * (this->poincare_x.at(i) - this->poincare_y.at(i)) / 2;
        SD2Temp += (this->poincare_x.at(i) + this->poincare_y.at(i) - 2*mean) * (this->poincare_x.at(i) + this->poincare_y.at(i) - 2*mean) / 2;
    }
    this->SD1 = sqrt(SD1Temp / poincareSize);
    this->SD2 = sqrt(SD2Temp / poincareSize);

    // temp - debug (pisanie do pliku)
    /*
    QFile plik("myoutput.txt");
    if (plik.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&plik);

        out << "r_peaks=[";
        for (int i=0; i<rPeaksSize; i++)
            out << this->rPeaks.at(i) << " ";
        out << "]; ";

        out << "tachogram_x_nc=[";
        for (int i=0; i<rIntervalsSize_nc; i++)
            out << i << " ";
        out << "]; ";
        out << "tachogram_y_nc=[";
        for (int i=0; i<rIntervalsSize_nc; i++)
            out << r_intervals_nc[i] << " ";
        out << "]; ";

        out << "tachogram_x=[";
        for (int i=0; i<rIntervalsSize; i++)
            out << i << " ";
        out << "]; ";
        out << "tachogram_y=[";
        for (int i=0; i<rIntervalsSize; i++)
            out << r_intervals[i] << " ";
        out << "]; ";

        out << "histogram_x=[";
        for (int i=0; i<histogramSize; i++)
            out << this->histogram_x.at(i) << " ";
        out << "]; ";
        out << "histogram_y=[";
        for (int i=0; i<histogramSize; i++)
            out << this->histogram_y.at(i) << " ";
        out << "]; ";

        out << "triangularIndex=" << this->triangularIndex << "; ";
        out << "TINN=" << this->TINN << "; ";

        int poincareSize = this->poincare_x.size();
        out << "poincare_x=[";
        for (int i=0; i<poincareSize; i++)
            out << this->poincare_x.at(i) << " ";
        out << "]; ";
        out << "poincare_y=[";
        for (int i=0; i<poincareSize; i++)
            out << this->poincare_y.at(i) << " ";
        out << "]; ";

        out << "SD1=" << this->SD1 << "; ";
        out << "SD2=" << this->SD2 << "; ";
    }
    */
}

int HRV2::DopasujWielokrotnoscSredniej(int srednia, double wartosc)
{
    int dopasowanie = 1;
    double odleglosc = abs(wartosc - srednia);
    while (odleglosc > abs(wartosc - (dopasowanie + 1) * srednia))
    {
        dopasowanie++;
        odleglosc = abs(wartosc - dopasowanie * srednia);
    }
    return dopasowanie;
}
