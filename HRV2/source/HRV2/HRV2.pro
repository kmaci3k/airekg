#-------------------------------------------------
#
# Project created by QtCreator 2012-12-25T21:59:34
#
#-------------------------------------------------

QT       -= gui

TARGET = HRV2
TEMPLATE = lib

CONFIG += shared

DEFINES += HRV2_LIBRARY

SOURCES += hrv2.cpp

HEADERS += hrv2.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
