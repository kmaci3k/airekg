#include "hrv2module.h"
#include <QMap>
#include <QTextStream>
#include <cmath>
#include "hrv2.h"

#define DEVELOP

//#ifdef DEVELOP
    #include "QDebug"
//#endif

Hrv2Module::Hrv2Module() : ProcessingModule()
{
}

void Hrv2Module::Process(EcgData *data)
{
//#ifdef DEVELOP
    qDebug() << "Hrv2Module";
//#endif

    // Zabezpieczenie przed wybuchem
    if(!data || !data->r_peaks || data->r_peaks->isEmpty() || data->r_peaks->size() == 0)
        return;

    // Tworzenie obiektu HRV2
    HRV2 module;

    // Dane wejściowe
    module.sampleFreq = (double)data->info->frequencyValue;

    int rPeaksSize = data->r_peaks->size();
    for (int i=0; i<rPeaksSize; i++)
        module.rPeaks.append(data->r_peaks->at(i));

    // Obliczenia
    module.Process();

    // Dane wyjściowe
    int histogramSize = module.histogram_x.size();
    QList<unsigned int> *histogramx = new QList<unsigned int>();
    histogramx->reserve(histogramSize);
    for (int i=0;i<histogramSize;i++) histogramx->append(module.histogram_x.at(i));
    data->histogram_x = histogramx;

    QList<int> *histogramy = new QList<int>();
    histogramy->reserve(histogramSize);
    for (int i=0;i<histogramSize;i++) histogramy->append(module.histogram_y.at(i));
    data->histogram_y = histogramy;

    int poincareSize = module.poincare_x.size();
    QList<unsigned int> *poincarex = new QList<unsigned int>();
    poincarex->reserve(poincareSize);
    for (int i=0;i<poincareSize;i++) poincarex->append(module.poincare_x.at(i));
    data->poincare_x = poincarex;

    QList<int> *poincarey = new QList<int>();
    poincarey->reserve(poincareSize);
    for (int i=0;i<poincareSize;i++) poincarey->append(module.poincare_y.at(i));
    data->poincare_y = poincarey;

    double *triangularIndex = new double;
    *triangularIndex = module.triangularIndex;
    data->triangularIndex = triangularIndex;

    double *TINN = new double;
    *TINN = module.TINN;
    data->TINN = TINN;

    double *SD1 = new double;
    *SD1 = module.SD1;
    data->SD1 = SD1;

    double *SD2 = new double;
    *SD2 = module.SD2;
    data->SD2 = SD2;
}
