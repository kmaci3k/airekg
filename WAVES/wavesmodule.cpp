#include "wavesmodule.h"

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#include <QFile>
#endif

WavesModule::WavesModule() : ProcessingModule()
{

}

void WavesModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "WavesModule";
#endif

    Waves waves = Waves(*data->ecg_baselined, *data->r_peaks, 360.0);

    QList<Waves::EcgFrame*> frames = waves.DetectWaves();

    QList<Waves::EcgFrame*> *output = new QList<Waves::EcgFrame*>();
    output->reserve(frames.size());
    for(unsigned int i = 0; i < frames.size(); i++)
    {
        output->append(frames.at(i));
    }

    data->waves = output;
}
