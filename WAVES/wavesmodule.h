#ifndef WAVESMODULE_H
#define WAVESMODULE_H

#include "Common/processingmodule.h"
#include "waves.h"

class WavesModule : public ProcessingModule
{
public:
    WavesModule();
    virtual void Process(EcgData *data);
};

#endif // WAVESMODULE_H
