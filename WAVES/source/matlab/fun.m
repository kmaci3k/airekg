function [ out ] = fun(signal, point, range)
%function used to calculate derative
   out = (sum(signal(point+1:point+range)) - sum(signal(point-range:point-1)))/range;
end