function [ out_onset, out_end, out, out2 ] = get_P_wave( signal, RR, QRS_ONSET )
%GET_P_END Summary of this function goes here
%   Detailed explanation goes here
    out_onset = ones(length(RR), 1);
    out_end = out_onset;
    out = zeros(length(signal),1);
    out2 = zeros(length(signal),1);
    for i=1:length(RR)
        window = ceil(RR(i)/4);
        QRS_ONS = QRS_ONSET(i+1);
        foundP_peak = false;
        while(~foundP_peak)
            window_signal = signal(QRS_ONS-window : QRS_ONS);
            PT = window_signal - median(window_signal);
            Rv = 0.003;
            PT = atan(PT/Rv);
            M = sqrt(Rv^2 + window_signal.^2);
            [P_peak_value, P_peak] = max(PT);
            if M(P_peak-1) < M(P_peak) && M(P_peak+1) < M(P_peak)
                foundP_peak = true;
                P_peak = P_peak + QRS_ONS - window - 1;
            else 
                window = window - 1;
            end
        end
        
        tmpms = 15;
        %left side
        P_ONSET_found = false;
        Rv = 0.005;
        while ~P_ONSET_found
            window_signal = signal(P_peak-tmpms : P_peak);
            P_left = window_signal - (max(window_signal) - min(window_signal))/2;
            PT_left = atan(P_left/Rv);
            dPT_left = PT_left;
            range = 3;
            for j=range:length(PT_left)-range
                dPT_left(j) = fun(PT_left, j, range-1); %(PT_left(j) - PT_left(j-1))/dt;
                if(dPT_left(j - 1) * dPT_left(j) < 0)
                    out_onset(i) = j + P_peak - tmpms - 1;
                    P_ONSET_found = true;
                end
            end
            out(P_peak-tmpms : P_peak) = PT_left;
            out2(P_peak-tmpms : P_peak) = dPT_left;
            Rv = Rv*0.8;
        end

        %right side
        P_END_found = false;
        Rv = 0.005;
        while ~P_END_found && Rv > 1e-10
            window_signal = signal(P_peak : QRS_ONS);
            P_right = window_signal - median(window_signal);
            PT_right = atan(P_right/Rv);
            dPT_right = PT_right;
            range = 4;
            for j=range:length(PT_right)-range
                dPT_right(j) = fun(PT_right, j, range-1); %(PT_left(j) - PT_left(j-1))/dt;
            end
            [tmp tmp_it] = min(dPT_right);

            for j=tmp_it:length(dPT_right)-1
                if(dPT_right(j) * dPT_right(j+1) <= 1e-5)
                    out_end(i) = j + P_peak - 2;
                    P_END_found = true;
                    break;
                end
            end
            Rv = Rv * 0.7;
        end
    end


end

