clear all;
close all;

signal = importdata('../out_test/100_ecg_baselined.txt');
R_peaks = importdata('../out_test/100_r_peaks_samples_1.txt');

MAX_TIME = 4;  %sec
dt = 1/360;     % ~2.8ms

signal = signal(1:(MAX_TIME/dt));
time = 0:dt:dt*(length(signal)-1);

%ograniczenie r_peaks do MAX_TIME
[a,b] = min(abs(R_peaks-length(signal)));
if(R_peaks(b) > MAX_TIME/dt)
    b = b-1;
end
%warto�ci zacyznaj� si� od 0, zatem nale�y przenumerowa�
R_peaks = R_peaks(1:b)+1;

[QRS_ONSET, Vs, A, s, VS, AS] = get_QRS_onset(signal, R_peaks, dt);
QRS_END = get_QRS_end(signal, R_peaks, Vs, A, s, VS, AS, dt);
[T_END, out_S, out_W, out_MMD, RR] = get_T_end(signal, R_peaks, QRS_END, dt);
[P_ONSET, P_END, tmp, tmp2] = get_P_wave(out_S, RR, QRS_ONSET);

time=0:1:length(time)-1;
figure(1);
% subplot(2,1,2);
% plot(time,tmp, time, tmp2);
% subplot(2,1,1);
% hold on;
% plot(time, signal);
% plot(time(QRS_ONSET), signal(QRS_ONSET), 'xk', 'LineWidth',3);
% plot(time(QRS_END), signal(QRS_END), 'xc', 'LineWidth',3);
% plot(time(T_END), signal(T_END),'xm','LineWidth',3);
% plot(time(P_ONSET), signal(P_ONSET),'xr','LineWidth',3);
% plot(time(P_END), signal(P_END),'xy','LineWidth',3);

plot(out_S);
ecg_filtered = importdata('../ecg_filtered.txt');
hold on;
plot(ecg_filtered, 'r')
% plot(time, out_S,'r');
% xlim([0 length(time)]);
%plot(time, out_W,'g');
%plot(time, out3,'k');
%plot(300);