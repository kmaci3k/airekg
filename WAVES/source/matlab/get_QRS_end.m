function [ out ] = get_QRS_end( signal, R_peaks, Vs, A, s, VS, AS, dt )
    
    %sta�e czasowe u�ywane w algorytmie
    ms15 = ceil(0.015/dt);
    ms20 = ceil(0.020/dt);
    ms32 = ceil(0.032/dt);
    ms50 = ceil(0.050/dt);
    ms200 = ceil(0.200/dt);
    
    QD = zeros(length(R_peaks), 1);
    Q_SAPR = QD;
    out = Q_SAPR;
    for i=1:length(R_peaks)
        Vmin = 2;
        Amax = 2;
        found = false;
        R_peak = R_peaks(i);
        while ~found
            counter = 0;
            for j = R_peak : R_peak + ms200
                if (abs(Vs(j)) < Vmin)
                    counter = counter + 1;
                    if (counter >= ms15)
                        QD(i) = j;
                        found = true;
                        break;
                    end
                else 
                    counter = 0;
                end
            end
            Vmin = Vmin + 1;
        end
        %Wyznaczenie Q_SAPR
        for j = QD(i) : -1 : R_peak
            if (A(j) > Amax)
                Q_SAPR(i) = j;
                break;
            end
        end
        %izolinia
        signal(Q_SAPR(i)+ms20 : Q_SAPR(i)) = signal(Q_SAPR(i)+ms20 : Q_SAPR(i)) - mean(signal(Q_SAPR(i)+ms20 : Q_SAPR(i)));
        s(Q_SAPR(i) - ms50 : Q_SAPR(i) + ms50) = abs(signal(Q_SAPR(i) - ms50 : Q_SAPR(i) + ms50));
        range = 5;
        for j = Q_SAPR(i) - ms50 + range : Q_SAPR(i) + ms50 - range
            VS(j) = fun(s, j, range-1);
        end
        for j = Q_SAPR(i) - ms50 + range : Q_SAPR(i) + ms50 - range
            AS(j) = fun(VS, j, range-1);
        end
        AS_min = min(AS(Q_SAPR(i) - ms32 : QD(i))); %punkty wcze�niejsze od QD na izolinii
        AS_max = max(AS(Q_SAPR(i) - ms32 : QD(i)));
        Ne = AS_max - AS_min + 2;
        Q_S1 = 0;
        for j = QD(i) : -1 : Q_SAPR(i) - ms32
            if (AS(j) > Ne) 
                Q_S1 = j;
                break;
            end
        end
        if (Q_S1 > 0)
            out(i) = Q_S1 + 1;
        else
            %sprintf(['Punkt przybli�ony ' int2str(i)]);
            out(i) = Q_SAPR(i);
        end
    end

end

