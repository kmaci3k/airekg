function [ out ] = dilation( signal, B )
%function that calculate the dilation with B element
    N = length(signal);
    M = length(B);
    out = signal;
    for i = 1 + (M-1)/2 : N - (M-1)/2
        out(i) = max( signal(i-(M-1)/2 : i-(M-1)/2 + M - 1) + B(1:M) );
    end;
end