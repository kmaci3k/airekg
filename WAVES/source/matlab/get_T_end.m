function [ out, out_S, out_W, out_MMD, RR ] = get_T_end(signal, R_peaks, QRS_END, dt)
%returns T_END points
%   out - T_END points
%   out_S - signal after morphological filtering
%   out_W - the 'wings' function
%   out_MMD - MMD tranform
%   out_RR - R-R interval

    B1 = [0 1 2 3 4 5 4 3 2 1 0]';
    B2 = zeros(11,1);
    S = (erosion(dilation(signal, B1),B2) + dilation(erosion(signal, B1),B2))./2;
    
    %median filtering
    m1=37;
    m2=253;
    tmp = S;
    for i = 1 + (m1-1)/2 : length(signal) - (m1-1)/2
        tmp(i) = mean(signal(i-(m1-1)/2 : i+(m1-1)/2));
    end
    tmp2 = tmp;
    for i = 1 + (m2-1)/2 : length(signal) - (m2-1)/2
        tmp2(i) = mean(tmp(i-(m2-1)/2 : i+(m2-1)/2));
    end
    S = S - tmp2;
    
    ms100 = ceil(0.1/dt);
    ms40 = ceil(0.04/dt);
    ms20 = ceil(0.02/dt);
    ms16 = ceil(0.016/dt);
    W = S;
    MMD = S;
    RR = zeros(length(R_peaks)-1, 1);
    out = ones(length(R_peaks), 1);
    for i=1:length(R_peaks)
        %ostatnie QT r�wne jest przedostatniemu
        if i < length(R_peaks)
            RR(i) = (R_peaks(i + 1) - R_peaks(i));
            QT = ceil(0.5*sqrt(RR(i)*dt)/dt);
        end
        J = QRS_END(i);
        if J + QT > length(S) -  ms40
            break;
        end
        for j = J : J + QT
            W(j) = (S(j - 15) - S(j))*(S(j) - S(j + 15));
        end
        [Tp1_value Tp1] = min(W(J + ms20 : ceil(J + 0.75 * QT)));
        Tp1 = Tp1 + ms20 + J - 1; %poprawka na pozycj� minimum
        [Steep_value steep] = max(W(Tp1 : Tp1 + ceil(QT/6)));
        steep = steep + Tp1 - 1; %poprawka na pozycj� minimum
        if abs(Steep_value) > 0.8*abs(Tp1_value)
            [Tp2_value Tp2] = min(W(steep : J + QT));
            Tp2 = Tp2 + steep - 1; %poprawka na pozycj� minimum
            if (S(Tp1)*S(Tp2) < 0) && (abs(S(Tp2)) > 0.2*abs(S(Tp1)))
                T_peak = Tp2;
            else
                T_peak = Tp1;
            end
        else
            T_peak = Tp1;
        end
        TR = J + QT;
        for j = T_peak + 20 : J + QT
            %mniejsze ni� 3uV
            if (S(j-2) - S(j)) * (S(j) - S(j+2)) < 0.003
                TR = j;
                break
            end
        end
        S_TL = 0.8*(S(TR) - S(T_peak)) + S(T_peak);
        TL = T_peak + ceil(TR - T_peak)/2;
        for j = T_peak : TR
            if S(j) > S_TL && S(T_peak) < S(TR)
                TL = j;
                break;
            elseif S(j) < S_TL && S(T_peak) > S(TR) 
                TL = j;
                break;
            end
        end
        sm = ceil((TL-T_peak)/0.8);
        for j = J : min(J + QT, length(S) - ms40)
            MMD(j) = (max(S(j - sm : j + sm)) + min(S(j - sm : j + sm)) - 2*S(j))/sm;
        end
        if S(T_peak) > 0
            [value T_END] = max(MMD(TL:TR));
        else
            [value T_END] = min(MMD(TL:TR));
        end
        out(i) = T_END + TL;
    end
    out_S = S;
    out_W = W;
    out_MMD = MMD;
end

