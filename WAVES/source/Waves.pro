QT       -= gui

TARGET = WAVES
TEMPLATE = lib
CONFIG += shared

DEFINES += WAVES_LIBRARY

SOURCES += waves.cpp

HEADERS += waves.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
