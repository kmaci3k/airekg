#include "waves.h"

Waves::Waves(QList<double> baselinedSignal, QList<unsigned int> rPeaks, double samplingFrequency)
{
    this->baselinedSignal = baselinedSignal;
    this->SPlane = baselinedSignal;
    this->SPlaneVelocities = baselinedSignal;
    this->SPlaneAccelerations = baselinedSignal;
    this->rPeaks = rPeaks;
    this->samplingFrequency = samplingFrequency;
    this->sampleTime = 1.0 / samplingFrequency;
    this->filteredSignal = baselinedSignal;

    this->ms15 = (int)ceil(0.015 / this->sampleTime);
    this->ms20 = (int)ceil(0.020 / this->sampleTime);
    this->ms32 = (int)ceil(0.032 / this->sampleTime);
    this->ms50 = (int)ceil(0.050 / this->sampleTime);
    this->ms200 = (int)ceil(0.2 / this->sampleTime);
    this->ms100 = (int)ceil(0.1 / this->sampleTime);
    this->ms40 = (int)ceil(0.040 / this->sampleTime);
    this->ms16 = (int)ceil(0.016 / this->sampleTime);

    CalculateVelocities();
    CalculateAccelerations();
    CalculateFilteredSignal();

    this->W = this->filteredSignal;
    this->MMD = this->filteredSignal;
}

Waves::~Waves()
{
    this->baselinedSignal.clear();
    this->SPlane.clear();
    this->SPlaneVelocities.clear();
    this->SPlaneAccelerations.clear();
    this->rPeaks.clear();
    this->filteredSignal.clear();
    this->W.clear();
    this->MMD.clear();
}

Waves::EcgFrame::EcgFrame()
{
}

unsigned int Waves::DetectQrs_onset(unsigned int rPeakIndex)
{
    // początkowa maksymalna wartość prędkości
    //double minVelocity = 2.0;

    unsigned int rPeak = this->rPeaks[rPeakIndex];

    unsigned int QD = rPeak;
    unsigned int Qsapr = QD;
    unsigned int output = Qsapr;
    bool wasIsolineFound = false;

    int counter = 0;

    for(int minVelocity = 2; ((minVelocity < 10) && (!wasIsolineFound)); minVelocity++)
    {
        for(unsigned int i = rPeak; i > rPeak - this->ms200; i--)
        {
            if(abs(this->velocities[i]) < minVelocity)
            {
                counter++;
                if(counter == this->ms15)
                {
                    QD = i;
                    wasIsolineFound = true;
                    break;
                }
            }
            else
            {
                counter = 0;
            }

        }
    }

    if(!wasIsolineFound)
    {
        return rPeak;
    }

    // poszukiwanie Q_SAPR
    double maxAcceleration = 2.0;
    for(unsigned int i = QD; i < rPeak; i++)
    {
        if(this->accelerations[i] > maxAcceleration)
        {
            Qsapr = i;
            break;
        }
    }

    // wyznaczanie izolinii
    double sum = 0;
    for(unsigned int i = Qsapr - this->ms20; i < Qsapr; i++)
    {
        sum += this->baselinedSignal[i];
    }
    double mean = sum / (double)this->ms20;

    for(unsigned int i = Qsapr - this->ms50; i < Qsapr + this->ms50; i++)
    {
        if((i >= Qsapr - this->ms20) && (i < Qsapr))
        {
            this->SPlane[i] = abs(this->baselinedSignal[i] - mean);
        }
        else
        {
            this->SPlane[i] = abs(this->baselinedSignal[i]);
        }
    }

    for(unsigned int i = Qsapr - this->ms50; i < Qsapr + this->ms50; i++)
    {
        this->SPlaneVelocities[i] = Differentiate(this->SPlane, i, 3);
    }

    for(unsigned int i = Qsapr - this->ms50; i < Qsapr + this->ms50; i++)
    {
        this->SPlaneAccelerations[i] = Differentiate(this->SPlaneVelocities,i,3);
    }

    double maxA = -9999;
    double minA = 9999;
    for(unsigned int i = Qsapr - this->ms32; i < QD; i++)
    {
        if(this->SPlaneAccelerations[i] > maxA)
        {
            maxA = this->SPlaneAccelerations[i];
        }
        if(this->SPlaneAccelerations[i] < minA)
        {
            minA = this->SPlaneAccelerations[i];
        }
    }

    double Ne = maxA - minA + 2.0;
    unsigned int S1 = 0;

    for(unsigned int i = QD; i < Qsapr + this->ms32; i++)
    {
        if(this->SPlaneAccelerations[i] > Ne)
        {
            S1 = i;
            break;
        }
    }

    if(S1 > 0)
    {
        output = S1 - 1;
    }
    else
    {
        output = Qsapr;
    }
    return output;
}

unsigned int Waves::DetectQrs_end(unsigned int rPeakIndex)
{
    unsigned int output;
    unsigned int rPeak = this->rPeaks[rPeakIndex];

    //double minVelocity = 2.0;

    unsigned int QD = rPeak;
    unsigned int Q_SAPR = QD;

    bool wasIsolineFound = false;

    int counter = 0;

    for(int minVelocity = 2; ((minVelocity < 10) && (!wasIsolineFound)); minVelocity++)
    {
        for(unsigned int i = rPeak; i < rPeak + this->ms200; i++)
        {
            if(abs(this->velocities[i]) < minVelocity)
            {
                counter++;
                if(counter == this->ms15)
                {
                    QD = i;
                    wasIsolineFound = true;
                    break;
                }
            }
            else
            {
                counter = 0;
            }
        }
    }

    if(!wasIsolineFound)
    {
        return rPeak;
    }

    // poszukiwanie Q_SAPR
    double maxAcceleration = 2.0;
    for(unsigned int i = QD; i > rPeak; i--)
    {
        if(this->accelerations[i] > maxAcceleration)
        {
            Q_SAPR = i;
            break;
        }
    }

    // wyznaczanie izolinii
    double sum = 0;
    for(unsigned int i = Q_SAPR + this->ms20; i > Q_SAPR; i--)
    {
        sum += this->baselinedSignal[i];
    }
    double mean = sum / (double)this->ms20;

    for(unsigned int i = Q_SAPR - this->ms50; i < Q_SAPR + this->ms50; i++)
    {
        if((i <= Q_SAPR + this->ms20) && (i > Q_SAPR))
        {
            this->SPlane[i] = abs(this->baselinedSignal[i] - mean);
        }
        else
        {
            this->SPlane[i] = abs(this->baselinedSignal[i]);
        }
    }

    for(unsigned int i = Q_SAPR - this->ms50 + 4; i < Q_SAPR + this->ms50 - 4; i++)
    {
        this->SPlaneVelocities[i] = Differentiate(this->SPlane, i, 4);
    }

    for(unsigned int i = Q_SAPR - this->ms50 + 4; i < Q_SAPR + this->ms50 - 4; i++)
    {
        this->SPlaneAccelerations[i] = Differentiate(this->SPlaneVelocities,i,4);
    }

    double maxA = -9999;
    double minA = 9999;
    for(unsigned int i = Q_SAPR - this->ms32; i < QD; i++)
    {
        if(this->SPlaneAccelerations[i] > maxA)
        {
            maxA = this->SPlaneAccelerations[i];
        }
        if(this->SPlaneAccelerations[i] < minA)
        {
            minA = this->SPlaneAccelerations[i];
        }
    }

    double Ne = maxA - minA + 2.0;
    unsigned int S1 = 0;

    for(unsigned int i = QD; i > Q_SAPR - this->ms32; i--)
    {
        if(this->SPlaneAccelerations[i] > Ne)
        {
            S1 = i;
            break;
        }
    }

    if(S1 > 0)
    {
        output = S1 + 1;
    }
    else
    {
        output = Q_SAPR;
    }

    return output;
}

unsigned int Waves::DetectT_end(unsigned int rPeakIndex, unsigned int Qrs_end)
{
    unsigned int output;
    unsigned int rPeak = this->rPeaks[rPeakIndex];
    double dt = this->sampleTime;

    unsigned int QT = 0.0;
    unsigned int T_Peak = 0;
    unsigned int J = Qrs_end;
    if(rPeakIndex == this->rPeaks.count() - 1)
    {
        QT = (unsigned int)ceil(0.5*sqrt(abs(rPeak - this->rPeaks[rPeakIndex - 1])*dt)/dt);
    }
    else
    {
        QT = (unsigned int)ceil(0.5*sqrt(abs(this->rPeaks[rPeakIndex + 1] - rPeak)*dt)/dt);
    }

    unsigned int loopEnd = 0;
    unsigned int loopStart = J;
    if(J + QT < this->filteredSignal.length() - ms40)
    {
        loopEnd = J + QT;
    }
    else
    {
        loopEnd = this->filteredSignal.length() - ms40;
    }

    if(J < 15)
    {
        loopStart = 15;
    }
    for(unsigned int i = loopStart; i < loopEnd; i++)
    {
        this->W[i] = (this->filteredSignal[i - 15] - this->filteredSignal[i]) *
                (this->filteredSignal[i] + this->filteredSignal[i + 15]);
    }

    unsigned int Tp1 = 0;
    unsigned int Tp2 = 0;
    double Tp1Value = 9999;
    double Tp2Value = 9999;

    for(unsigned int i = J + ms20; i < ceil(J + 0.75 * QT); i++)
    {
        if(this->W[i] < Tp1Value)
        {
            Tp1Value = this->W[i];
            Tp1 = i;
        }
    }

    unsigned int steep = 0;
    double steepValue = -9999;

    for(unsigned int i = Tp1; i < Tp1 + ceil(QT / 6.0); i++)
    {
        if(this->W[i] > steepValue)
        {
            steepValue = this->W[i];
            steep = i;
        }
    }

    if(abs(steepValue) > 0.8 * abs(Tp1Value))
    {
        for(unsigned int i = steep; i < J + QT; i++)
        {
            if(this->W[i] < Tp2Value)
            {
                Tp2Value = this->W[i];
                Tp2 = i;
            }
        }

        if((this->filteredSignal[Tp1]*this->filteredSignal[Tp2] < 0) &&
                (abs(this->filteredSignal[Tp2]) > 0.2 * abs(this->filteredSignal[Tp1])))
        {
            T_Peak = Tp2;
        }
        else
        {
            T_Peak = Tp1;
        }
    }
    else
    {
        T_Peak = Tp1;
    }

    unsigned int TR = J + QT;
    unsigned int TRloopEnd = TR;
    if(TR > this->filteredSignal.size() - 3)
    {
        TR = this->filteredSignal.size() - 3;
        TRloopEnd = TR;
    }

    for(unsigned int i = T_Peak + ms16; i < TRloopEnd; i++)
    {
        if((this->filteredSignal[i - 2] - this->filteredSignal[i]) *
                (this->filteredSignal[i] - this->filteredSignal[i+2]) < 0.003)
        {
            TR = i;
            //break;
        }
    }

    double S_TL = 0.8*(this->filteredSignal[TR] - this->filteredSignal[T_Peak]) + this->filteredSignal[T_Peak];
    long int TL = (long int)T_Peak + (long int)ceil((double)TR - (double)T_Peak)/2.0;

    for(unsigned int i = T_Peak; i < TR; i++)
    {
        if((this->filteredSignal[i] > S_TL) && (this->filteredSignal[T_Peak] < this->filteredSignal[TR]))
        {
            TL = i;
            break;
        }
        else if((this->filteredSignal[i] < S_TL) && (this->filteredSignal[T_Peak] > this->filteredSignal[TR]))
        {
            TL = i;
            break;
        }
    }

    long int sm = ceil(((double)TL - (double)T_Peak)/0.8);
    double maxS;
    double minS;
    if(loopEnd + sm >= this->filteredSignal.size())
    {
        loopEnd = this->filteredSignal.size() - sm - 1;
    }
    for(unsigned int i = J; i < loopEnd; i++)
    {
        maxS = this->filteredSignal[i];
        minS = this->filteredSignal[i];
        for(unsigned int j = i - sm; j < i + sm; j++)
        {
            if(this->filteredSignal[j] < minS)
            {
                minS = this->filteredSignal[j];
            }
            if(this->filteredSignal[j] > maxS)
            {
                maxS = this->filteredSignal[j];
            }
        }
        this->MMD[i] = (maxS + minS - 2 * this->filteredSignal[i])/((double)sm);
    }

    unsigned int T_End = Qrs_end;
    double maxMMD = this->MMD[TL];
    double minMMD = this->MMD[TL];
    if(this->filteredSignal[T_Peak] > 0)
    {
        for(unsigned int i = TL; i < TR; i++)
        {
            if(this->MMD[i] > maxMMD)
            {
                maxMMD = this->MMD[i];
                T_End = i;
            }
        }
    }
    else
    {
        for(unsigned int i = TL; i < TR; i++)
        {
            if(this->MMD[i] < minMMD)
            {
                minMMD = this->MMD[i];
                T_End = i;
            }
        }
    }
    output = T_End;
    return output;
}

void Waves::DetectP_wave(unsigned int *P_onset, unsigned int *P_end , unsigned int rPeakIndex, unsigned int Qrs_onset)
{
    unsigned int RR;
    unsigned int onsetOut = 1;
    unsigned int endOut = 1;
    unsigned int rPeak = this->rPeaks[rPeakIndex];

    if(rPeakIndex == this->rPeaks.count() - 1)
    {
        RR = abs(rPeak - this->rPeaks[rPeakIndex - 1]);
    }
    else
    {
        RR = abs(rPeak - this->rPeaks[rPeakIndex - 1]);
    }

    unsigned int windowWidth = (unsigned int)ceil(RR/4.0);
    QList<double> windowSignal;
    QList<double> M;
    double pPeakValue = -9999;
    unsigned int pPeak = 0;
    double rv = 0.003;
    double sumTemp = 0;
    bool pPeakFound = false;
    while((!pPeakFound)&&(windowWidth > 4))
    {
        windowSignal.clear();
        M.clear();
        sumTemp = 0;
        for(unsigned int i = Qrs_onset - windowWidth; i < Qrs_onset; i++)
        {
            windowSignal.append(this->filteredSignal[i]);
            sumTemp = sumTemp + this->filteredSignal[i];
        }

        double median = sumTemp / (double)windowWidth;

        for(unsigned int i = 0; i < windowSignal.count(); i++)
        {
            double at = atan((windowSignal[i] - median)/rv);
            if(at > pPeakValue)
            {
                pPeakValue = at;
                pPeak = i;
            }
            M.append(sqrt(rv*rv + windowSignal[i]*windowSignal[i]));
        }

        if(pPeak == M.count() - 1)
        {
            pPeak = pPeak - 1;
        }
        if(pPeak < 1)
        {
            pPeak = 1;
        }

        if((M[pPeak - 1] < M[pPeak] && (M[pPeak + 1] < M[pPeak])))
        {
            pPeakFound = true;
            pPeak = pPeak + Qrs_onset - windowWidth - 1;
            break;
        }
        else
        {
            windowWidth = windowWidth - 1;
        }
    }

    if(!pPeakFound)
    {
        *P_onset = Qrs_onset;
        *P_end = Qrs_onset;
        return;
    }

    unsigned int tmpms = 15;
    bool pOnsetFound = false;
    rv = 0.005;
    QList<double> PT;
    double last_dPT = 0;
    double dPT = 0;
    double median;
    while((!pOnsetFound)&&(rv>0.0000000001))
    {
        windowSignal.clear();
        PT.clear();
        last_dPT = 0;
        sumTemp = 0;
        for(unsigned int i = pPeak - tmpms; i < pPeak; i++)
        {
            windowSignal.append(this->filteredSignal[i]);
            sumTemp = sumTemp + this->filteredSignal[i];
        }

        median = sumTemp / (double)tmpms;

        for(unsigned int i = 0; i < windowSignal.count(); i++)
        {
            PT.append(atan((windowSignal[i] - median)/rv));
        }

        for(unsigned int i = 2; i < windowSignal.count()-2; i++)
        {
            dPT = Differentiate(PT, i, 2); //toggle breakpoint
            if(dPT * last_dPT <= 0.00001)
            {
                onsetOut = i + pPeak - tmpms - 1;
                pOnsetFound = true;
                break;
            }
            else
            {
                last_dPT = dPT;
            }
        }
        rv = rv * 0.8;
    }


    bool pEndFound = false;
    rv = 0.005;
    QList<double> dPTList;
    double dptValue;
    double dptIndex;
    while((!pEndFound)&&(rv>0.0000000001))
    {
        windowSignal.clear();
        PT.clear();
        dPTList.clear();
        dptValue = 9999;
        dptIndex = 0;
        sumTemp = 0;
        for(unsigned int i = pPeak; i < Qrs_onset; i++)
        {
            windowSignal.append(this->filteredSignal[i]);
            sumTemp = sumTemp + this->filteredSignal[i];
        }

        double median = sumTemp / (double)(Qrs_onset - pPeak);

        for(unsigned int i = 0; i < windowSignal.count(); i++)
        {
            PT.append(atan((windowSignal[i] - median)/rv));
        }

        dPTList = PT;
        for(unsigned int i = 3; i < PT.count()-3; i++)
        {
            dPTList.replace(i, Differentiate(PT, i, 3));
        }

        for(unsigned int i = 0; i < dPTList.count(); i++)
        {
            if(dPTList[i] < dptValue)
            {
                dptValue = dPTList[i];
                dptIndex = i;
            }
        }

        for(unsigned int i = dptIndex; i < dPTList.count() - 1; i++)
        {
            if(dPTList[i] * dPTList[i+1] <= 0.00001)
            {
                endOut = i + pPeak - 2;
                pEndFound = true;
            }
        }
        rv = rv * 0.7;
    }
    *P_onset = onsetOut;
    *P_end = endOut;
}

QList<double> Waves::Erosion(QList<double> signal, QList<int> B)
{
    unsigned int N = signal.count();
    unsigned int M = B.count();
    QList<double> output = signal;
    unsigned int last = N - (M+1)/2;
    double minValue;
    double temp;
    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        minValue = 9999;
        for(unsigned int j = 0; j < M; j++)
        {
            temp = signal[i - (M - 1)/2 + j] - B[j];
            if(temp < minValue)
            {
                minValue = temp;
            }
        }
        output.replace(i, minValue);
    }
    return output;
}

QList<double> Waves::Dilation(QList<double> signal, QList<int> B)
{
    unsigned int N = signal.count();
    unsigned int M = B.count();
    QList<double> output = signal;
    unsigned int last = N - (M+1)/2;
    double maxValue;
    double temp;
    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        maxValue = -9999;
        for(unsigned int j = 0; j < M; j++)
        {
            temp = signal[i - (M - 1)/2 + j] + B[j];
            if(temp > maxValue)
            {
                maxValue = temp;
            }
        }
        output.replace(i, maxValue);
    }
    return output;
}

void Waves::CalculateVelocities()
{
    int diffRange = 5;
    this->velocities.clear();
    // zredukowany zasieg dla poczatkowych probek
    for(int i = 0; i < diffRange; i++)
    {
        this->velocities.append(Differentiate(this->baselinedSignal, i, i));
    }
    // próbki o pełnym zasięgu
    for(unsigned int i = diffRange; i < this->baselinedSignal.count() - diffRange; i++)
    {
        this->velocities.append(Differentiate(this->baselinedSignal, i, diffRange));
    }
    // zredukowany zasieg dla końcowych probek
    for(unsigned int i = this->baselinedSignal.count() - diffRange; i < this->baselinedSignal.count(); i++)
    {
        this->velocities.append(Differentiate(this->baselinedSignal, i, this->baselinedSignal.count() - i -1));
    }
}

void Waves::CalculateAccelerations()
{
    int diffRange = 5;
    // zredukowany zasieg dla poczatkowych probek
    this->accelerations.clear();
    for(int i = 0; i < diffRange; i++)
    {
        this->accelerations.append(Differentiate(this->baselinedSignal, i, i));
    }
    // próbki o pełnym zasięgu
    for(unsigned int i = diffRange; i < this->baselinedSignal.count() - diffRange; i++)
    {
        this->accelerations.append(Differentiate(this->baselinedSignal, i, diffRange));
    }
    // zredukowany zasieg dla końcowych probek
    for(unsigned int i = this->baselinedSignal.count() - diffRange; i < this->baselinedSignal.count(); i++)
    {
        this->accelerations.append(Differentiate(this->baselinedSignal, i, this->baselinedSignal.count() - i - 1));
    }
}

void Waves::CalculateFilteredSignal()
{
    QList<int> B1;
    B1.append(0); B1.append(1); B1.append(2); B1.append(3); B1.append(4); B1.append(5);
    B1.append(4); B1.append(3); B1.append(2); B1.append(1); B1.append(0);

    QList<int> B2;
    for(int i = 0; i < 11; i++)
    {
        B2.append(0);
    }
 /*   QList<double> temp = Dilation(this->baselinedSignal, B1);
    QList<double> open = Erosion(temp, B2);
    temp = Erosion(this->baselinedSignal, B1);
    QList<double> close = Dilation(temp, B2);
    for(unsigned int i = 0; i < this->filteredSignal.count(); i++)
    {
        this->filteredSignal[i] = (open.at(i) + close.at(i))/2.0;
    }

*/

    QList<double> helper = this->baselinedSignal;

    unsigned int N = this->baselinedSignal.count();
    unsigned int M = B1.count();
    unsigned int last = N - (M+1)/2;
    double maxValue;
    double tempSignal;
    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        maxValue = -9999;
        for(unsigned int j = 0; j < M; j++)
        {
            tempSignal = this->baselinedSignal[i - (M - 1)/2 + j] + B1[j];
            if(tempSignal > maxValue)
            {
                maxValue = tempSignal;
            }
        }
        helper.replace(i, maxValue);
    }

    M = B2.count();
    last = N - (M+1)/2;
    double minValue;
    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        minValue = 9999;
        for(unsigned int j = 0; j < M; j++)
        {
            tempSignal = helper[i - (M - 1)/2 + j] - B2[j];
            if(tempSignal < minValue)
            {
                minValue = tempSignal;
            }
        }
        this->filteredSignal.replace(i, minValue);
    }

    helper.clear();
    helper = this->baselinedSignal;

    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        minValue = 9999;
        for(unsigned int j = 0; j < M; j++)
        {
            tempSignal = this->baselinedSignal[i - (M - 1)/2 + j] - B2[j];
            if(tempSignal < minValue)
            {
                minValue = tempSignal;
            }
        }
        helper.replace(i, minValue);
    }

    M = B1.count();
    last = N - (M+1)/2;
    for(unsigned int i =(M - 1)/2; i <= last; i++)
    {
        maxValue = -9999;
        for(unsigned int j = 0; j < M; j++)
        {
            tempSignal = helper[i - (M - 1)/2 + j] + B1[j];
            if(tempSignal > maxValue)
            {
                maxValue = tempSignal;
            }
        }
        this->filteredSignal[i] = (this->filteredSignal[i] + maxValue)/2.0;
    }


    int m1 = 37;
    int m2 = 253;
    QList<double> temp = this->filteredSignal;
    double sumTemp = 0;
    for(unsigned int i = (m1-1)/2; i < this->filteredSignal.count() - (m1-1)/2; i++)
    {
        sumTemp = 0;
        for(unsigned int j = i-(m1-1)/2; j < i+(m1-1)/2; j++)
        {
            sumTemp = sumTemp + this->filteredSignal[j];
        }
        temp[i] = (double)sumTemp / (double)((m1-1)/2);
    }

    QList<double> temp2 = temp;

    for(unsigned int i = (m2-1)/2; i < temp.count() - (m2-1)/2; i++)
    {
        sumTemp = 0;
        for(unsigned int j = i-(m2-1)/2; j < i+(m2-1)/2; j++)
        {
            sumTemp = sumTemp + temp[j];
        }
        temp2[i] = (double)sumTemp / (double)((m2-1)/2);
    }

    for(unsigned int i = 0; i < this->filteredSignal.count(); i++)
    {
        this->filteredSignal[i] = this->filteredSignal[i] - temp2[i];
    }
}

double Waves::Differentiate(QList<double> list, unsigned int point, unsigned int range)
{
    double output = 0;
    if(range == 0)
    {
        return list.at(point);
    }
    for(unsigned int i = point + 1; i <= point + range; i++)
    {
        output += list.at(i);
    }
    for(unsigned int i = point - range ; i <= point - 1; i++)
    {
        output -= list.at(i);
    }
    return output / range;
}

QList<Waves::EcgFrame*> Waves::DetectWaves()
{
    QList<EcgFrame*> output;
    for(unsigned int i = 0; i < this->rPeaks.count() - 1; i++)
    {
        EcgFrame *frame = new EcgFrame();
        frame->QRS_onset = DetectQrs_onset(i);
        frame->QRS_end = DetectQrs_end(i);
        frame->T_end = DetectT_end(i, frame->QRS_end);
        frame->P_end = 1;
        frame->P_onset = 1;
        output.append(frame);
    }

    for(unsigned int i = 1; i< output.count() - 1; i++)
    {
        unsigned int *onset = new unsigned int();
        unsigned int *end = new unsigned int();
        DetectP_wave(onset, end, i, output.at(i)->QRS_onset);
        output.at(i)->P_onset = *onset;
        output.at(i)->P_end = *end;
        delete onset;
        delete end;
    }
    return output;
}

