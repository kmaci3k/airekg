#ifndef HRTMODULE_H
#define HRTMODULE_H

#include "Common/processingmodule.h"
#include "source/hrt.h"

class HrtModule : public ProcessingModule
{
public:
    HrtModule();
    virtual void Process(EcgData *data);
private:
    QList<double> findSamplesNrOfPeaks(QList<unsigned int> *rPeaks);
    QList<double> findVPBs(QList<double> intervalDurations);

    double calculateTurbulenceOnset(QList<double> listNrOfVPBs, QList<double> intervalsDurations);
    double calculateTurbulenceSlope(double frequencyValue, QList<double> samplesNrOfPeaks, QList<double> listNrOfVPBs, QList<double> intervalsDurations);

    QList<unsigned int> filterSignal(unsigned int vpb, QList<unsigned int> rPeaks, QList<EcgAnnotation> annotations);
    bool isIntervalValid(QList<double> intervalDurations, int intervalNumber);
    int computeTimeDelta(QString start, QString end);
    QList<double> getIntervalsDurations(QList<double> intervals, double frequency);

    QList<double> generateTachogram(QList<unsigned int> *rPeaks, double frequencyValue);
    double linearRegressionSlope(QList<double> values);
    QList<double> findSteepestSlope(EcgData *data);

    QList<double> filterSamples(QList<double> listNrOfVPBs, QList<double> intervalsDurations);

};

#endif // HRTMODULE_H
