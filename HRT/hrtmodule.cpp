#include "hrtmodule.h"
#include <cstddef>

#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

HrtModule::HrtModule() : ProcessingModule()
{
}

void HrtModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "HrtModule";
#endif

    QList<double> samplesNrOfPeaks = findSamplesNrOfPeaks(data->r_peaks);
    QList<double> intervalsDurations = getIntervalsDurations(samplesNrOfPeaks, (double)data->info->frequencyValue);
    QList<double> listNrOfVPBs = HrtModule::findVPBs(intervalsDurations);

    data->turbulence_onset = new double(calculateTurbulenceOnset(listNrOfVPBs, intervalsDurations));
    //qDebug() << "TO=" << data->turbulence_onset << endl;
    data->turbulence_slope = new double(calculateTurbulenceSlope((double)data->info->frequencyValue, samplesNrOfPeaks,\
                                                                 listNrOfVPBs, intervalsDurations));
   // qDebug() << "TS=" << data->turbulence_slope << endl;

    data->hrt_tachogram = new QList<double>(generateTachogram(data->r_peaks, data->info->frequencyValue));

    data->hrt_tachogram_filtered_samples = new QList<double>();

    data->VPBs = new QList<double>(listNrOfVPBs);
}

QList<double> HrtModule::filterSamples(QList<double> listNrOfVPBs, QList<double> intervalsDurations)
{
    QList<double> filteredOutSamplesNrs;

    for (int i=0; i<listNrOfVPBs.size()-1; i++)
    {
        for (int j=listNrOfVPBs.at(i); j<listNrOfVPBs.at(i+1); j++)
        {
            int processedIntervals = 0;
            for (int k=j; k<listNrOfVPBs.at(i+1) && processedIntervals<5; k++)
            {
                if (! isIntervalValid(intervalsDurations, k))
                {
                    filteredOutSamplesNrs.push_back(k);
                    continue;
                }
                processedIntervals++;
            }
        }
    }
    return filteredOutSamplesNrs;
}

QList<double> HrtModule::findSamplesNrOfPeaks(QList<unsigned int> *rPeaks)
{
    QList<double> samplesNrOfPeaks;

    for(int i=0; i<rPeaks->size(); i++)
    {
        samplesNrOfPeaks.append(rPeaks->at(i));  //czasy kolejnych interwalow
    }

    return samplesNrOfPeaks;
}

QList<double> HrtModule::findVPBs(QList<double> intervalDurations)
{
    QList<double> listNrOfVPBs;
    for (int i=5; i<intervalDurations.size()-1; i++)
    {
        double currentIntervalDuration = intervalDurations.at(i);
        double nextIntervalDuration = intervalDurations.at(i+1);
        double referenceIntervalDuration = 0;
        for (int j=1; j<6; j++)
        {
            referenceIntervalDuration += intervalDurations.at(i-j);
        }
        referenceIntervalDuration /= 5;

        if ((currentIntervalDuration/referenceIntervalDuration < 0.8) &&
                (nextIntervalDuration/referenceIntervalDuration > 1.2))
        {
            if (listNrOfVPBs.size() > 0 && i-listNrOfVPBs.last() < 15)
                listNrOfVPBs.pop_back();
            listNrOfVPBs.append(i);
        }
    }

    return listNrOfVPBs;
}


double HrtModule::calculateTurbulenceOnset(QList<double> listNrOfVPBs, QList<double> intervalsDurations){
    QList<double> turbulenceOnsets;
    // oblicz turbulence onset
    for (int i=0; i<listNrOfVPBs.size(); i++)
    {
        int plc = listNrOfVPBs.at(i);

        if (plc > intervalsDurations.size() - 5 || plc < 4)
            continue;

        // RR1, RR2, RR-1, RR-2 pobierz z intervalsDurations
        double turbulenceOnset = 100 * (((intervalsDurations.at(plc+2+1) + intervalsDurations.at(plc+2+2)) - \
                                  (intervalsDurations.at(plc-1-1) + intervalsDurations.at(plc-1-2))) / \
                                  (intervalsDurations.at(plc-1-1) + intervalsDurations.at(plc-1-2)));

        if (turbulenceOnset < 0 )
            turbulenceOnsets.append(turbulenceOnset);
    }

    if(turbulenceOnsets.size()==0)
        return 0;

    double result = 0;
    for (int i=0; i<turbulenceOnsets.size(); i++)
    {
        result += turbulenceOnsets.at(i);
    }
    result /= turbulenceOnsets.size();

    return result;
}

// zaczerpniete z hrv1module
// funkcja zwracajaca dlugosci zalamkow [ms] na podstawie ich kolejnych czasow pojawiania sie
QList<double> HrtModule::getIntervalsDurations(QList<double> intervals, double frequency)
{
    double tempDuration;
    QList<double> intervalsDurations;
    for(int i=0; i<intervals.size()-1; i++)   //ilosc roznic jest mniejsza o jeden od wszystkich czasow zanotowania peakow
    {
        tempDuration = intervals.at(i+1)-intervals.at(i); // roznica czasowa miedzy kolejnymi interwalami
        tempDuration = tempDuration/frequency*1000;
        intervalsDurations.append(tempDuration);
    }
    return intervalsDurations;
}

bool HrtModule::isIntervalValid(QList<double> intervalDurations, int intervalNumber)
{
    double currentIntervalDuration = intervalDurations.at(intervalNumber);
    double previousIntervalDuration = intervalDurations.at(intervalNumber-1);
    double referenceIntervalDuration = 0;
    for (int j=1; j<6; j++)
    {
        referenceIntervalDuration += intervalDurations.at(intervalNumber-j);
    }
    referenceIntervalDuration = referenceIntervalDuration / 6;

    if (currentIntervalDuration > 300 &&
            currentIntervalDuration < 2000 &&
            abs(currentIntervalDuration - previousIntervalDuration) < 200 &&
            ((referenceIntervalDuration - currentIntervalDuration)/referenceIntervalDuration < 0.2) &&
            ((currentIntervalDuration - referenceIntervalDuration)/currentIntervalDuration < 0.2)
            )
    {
        return true;
    }
    return false;
}


// turbulence slope
double HrtModule::calculateTurbulenceSlope(double frequencyValue, QList<double> samplesNrOfPeaks, QList<double> listNrOfVPBs,\
                                           QList<double> intervalsDurations)
{
    //TS corresponds to the steepest slope of the linear regression line for each sequence of five consecutive normal intervals in the local tachogram
    //The Turbulence Slope calculations are based on the averaged tachogram and expressed in ms per RR interval

    // od nr probki z pvc kolejno wyznaczaj wspolczynniki regresji po 5 probek
    // wybierz ta o najwiekszym wspolczynniku

    // znajdz vpb
    // przeiteruj przedzialy pomiedzy kolejnymi vpb, co 5 probek obliczajac wsp regresji liniowej
    // dla przefiltrowanych danych

    double maxSlope = 0;
    for (int i=0; i<listNrOfVPBs.size()-1; i++)
    {
        for (int j=listNrOfVPBs.at(i); j<listNrOfVPBs.at(i+1); j++)
        {
            QList<double> values;

            int processedIntervals = 0;
            for (int k=j; k<listNrOfVPBs.at(i+1) && processedIntervals<5; k++)
            {
                if (! isIntervalValid(intervalsDurations, k))
                    continue;
                processedIntervals++;
                values.append(intervalsDurations.at(k));
            }

            if (processedIntervals != 5)
                continue;

            double currentSlope = linearRegressionSlope(values);

            if (currentSlope>maxSlope && currentSlope > 2.5)
                maxSlope = currentSlope;
        }
    }

    return maxSlope;
}

// regresja liniowa - wspolczynnik nachylenia
double HrtModule::linearRegressionSlope(QList<double> values)
{
    double s=5, sx=0, sy=0, sxx=0, sxy=0, syy=0, delta=0;
    double a=0, b=0;

    for (int i=0; i<5; i++)
    {
        sx += i;
        sy += values.at(i);
        sxx += i*i;
        sxy += i * values.at(i);
        syy += values.at(i) * values.at(i);
    }

    delta = s * sxx - sx * sx;
    a = (s * sxy - sx * sy) / delta;
    b = (sxx * sy - sx *sxy) / delta;

    return a;
}

QList<double> HrtModule::generateTachogram(QList<unsigned int> *rPeaks, double frequencyValue)
{
    QList<double> samplesNrOfPeaks;
    int intervalsSize = rPeaks->size();
    for(int i=0; i<intervalsSize; i++)
    {
        samplesNrOfPeaks.append(rPeaks->at(i));  //czasy kolejnych interwalow
    }

    QList<double> intervalsDurations = getIntervalsDurations(samplesNrOfPeaks, frequencyValue);
    return intervalsDurations;
}
