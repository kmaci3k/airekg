#ifndef ECG_BASELINEMODULE_H
#define ECG_BASELINEMODULE_H

#include "Common/processingmodule.h"

class Ecg_BaselineModule : public ProcessingModule
{
public:
    Ecg_BaselineModule();
    virtual void Process(EcgData *data);
};

#endif // ECG_BASELINEMODULE_H
