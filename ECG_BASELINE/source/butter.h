#pragma once

#include "ECG_BASELINE_global.h"

#include <QVector>
#include <complex>

typedef std::complex<double> complex;

// Second-order (biquadratic) IIR filtering
ECG_BASELINESHARED_EXPORT QVector<double> sosfilt(const QVector<double>& sosMatrix,
                                                  double gain,
                                                  const QVector<double>& x);
// Second-order (biquadratic) IIR Zero-phase digital filtering
ECG_BASELINESHARED_EXPORT QVector<double> sosfiltfilt(const QVector<double>& sosMatrix,
                                                      float gain,
                                                      const QVector<complex>& poles,
                                                      const QVector<double>& x);
ECG_BASELINESHARED_EXPORT QVector<double> processButter(const QVector<double>& signal);
