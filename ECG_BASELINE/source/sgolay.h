#pragma once

#include <QVector>

#include "ECG_BASELINE_global.h"

ECG_BASELINESHARED_EXPORT QVector<double> processSGolay(const QVector<double>& ecgData,
                                                        const int deg = 3,
                                                        const int frame = 500,
                                                        QVector<double>* baselineModel = 0);
