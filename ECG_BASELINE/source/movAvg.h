#pragma once

#include <QVector>
#include "ECG_BASELINE_global.h"

ECG_BASELINESHARED_EXPORT QVector<double> processMovAvg(const QVector<double>& signal, int windowSize);

// ECG_BASELINESHARED_EXPORT QVector<double> processModMovAvg(const QVector<double>& signal, int windowSize, int samplingRate);
