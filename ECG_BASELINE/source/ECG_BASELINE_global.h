#pragma once
#include <QtCore/qglobal.h>

#if defined(ECG_BASELINE_LIBRARY)
#  define ECG_BASELINESHARED_EXPORT Q_DECL_EXPORT
#else
#  define ECG_BASELINESHARED_EXPORT Q_DECL_IMPORT
#endif
