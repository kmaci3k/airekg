#-------------------------------------------------
#
# Project created by QtCreator 2012-11-19T22:34:59
#
#-------------------------------------------------

QT       -= gui

TARGET = ECG_BASELINE
TEMPLATE = lib
CONFIG += shared

DEFINES += ECG_BASELINE_LIBRARY

SOURCES += \
    butter.cpp \
    movAvg.cpp \
	sgolay.cpp \

HEADERS += ecg_baseline.h\
        ECG_BASELINE_global.h \
    butter.h \
    movAvg.h \
	sgolay.h 
	
INCLUDEPATH += .
QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
