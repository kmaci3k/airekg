#include "movAvg.h"

QVector<double> processMovAvg(const QVector<double>& signal, int windowSize){

    QVector<double> xx;
    int signalSize = signal.size();
    xx.reserve(signalSize);
    xx.append(signal[0]);
    //obliczenie sredniej kroczacej sygnalu o dlugosi okna windowSize
    for(int i = 1;i < signalSize; i++){
        if (i < windowSize){
            double result = (xx[i-1] * i + signal[i]) / (i+1);
            xx.append( result );
        }
        else{
            double result = xx[i-1] + (signal[i] - signal[i-windowSize]) / windowSize;
            xx.append( result );
        }
    }
    //odjecie skladowej stalej od sygnalu
    for(int i = 0;i < signalSize; i++){
        xx[i] = signal[i]-xx[i];
    }

    return xx;
}

/*
QVector<double> processModMovAvg(const QVector<double>& signal, int windowSize, int samplingRate){

    QVector<double> xx;
    int signalSize = signal.size();
    xx.reserve(signalSize);
    xx.append(signal[0]);
    int step = samplingRate;
    //obliczenie sredniej kroczacej sygnalu o dlugosi okna windowSize
    for(int i = 1;i < signalSize; i++){
        if (i < windowSize){
            double result = (xx[i-1] * i + signal[i]) / (i+1);
            xx.append( result );
        }
        else{
            double result = xx[i-1] + (signal[i] - signal[i-windowSize]) / windowSize;
            xx.append( result );
        }
    }
    //odjecie skladowej stalej od sygnalu
    for(int i = 0;i < signalSize; i++){
        xx[i] = signal[i]-xx[i];
    }

    return xx;
}
*/
