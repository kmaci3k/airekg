#include "ecg_baselinemodule.h"

#define DEVELOP

#ifdef DEVELOP
#  include "QDebug"
#endif

#include "ecg_baseline.h"

Ecg_BaselineModule::Ecg_BaselineModule() : ProcessingModule()
{
}

void Ecg_BaselineModule::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "Ecg_BaselineModule";
#endif

    if(!data || !data->GetCurrentSignal() || data->GetCurrentSignal()->isEmpty())
        return;

    QVector<double> signal(data->GetCurrentSignal()->size());
    qCopy(data->GetCurrentSignal()->begin(), data->GetCurrentSignal()->end(), signal.begin());
    QVector<double> y;

    switch(data->settings->EcgBaselineMode)
    {
    case 0:
        y = processButter(signal);
        break;
    case 1:
        y = processMovAvg(signal,150);
        break;
    case 2:
        y = processSGolay(signal,3,500);
        break;
    }

    data->ecg_baselined = new QList<double>();
    data->ecg_baselined->reserve(y.size());
    for(int i = 0; i < y.size(); ++i)
        data->ecg_baselined->append(y[i]);
}
