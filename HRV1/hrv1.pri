HEADERS += \
    HRV1/hrv1module.h \
    HRV1/stdafx.h \
    HRV1/specialfunctions.h \
    HRV1/solvers.h \
    HRV1/optimization.h \
    HRV1/linalg.h \
    HRV1/interpolation.h \
    HRV1/integration.h \
    HRV1/ap.h \
    HRV1/alglibmisc.h \
    HRV1/alglibinternal.h

SOURCES += \
    HRV1/hrv1module.cpp \
    HRV1/specialfunctions.cpp \
    HRV1/solvers.cpp \
    HRV1/optimization.cpp \
    HRV1/linalg.cpp \
    HRV1/interpolation.cpp \
    HRV1/integration.cpp \
    HRV1/ap.cpp \
    HRV1/alglibmisc.cpp \
    HRV1/alglibinternal.cpp
