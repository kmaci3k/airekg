#include "hrv1module.h"

#include "list"
#include "QList"
#include "cmath"
#include <complex>

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "interpolation.h"

using namespace alglib;


#define DEVELOP

#ifdef DEVELOP
    #include "QDebug"
#endif

//using namespace std;

Hrv1Module::Hrv1Module() : ProcessingModule()
{

}

// wartosc srednia dlugosci interwalow
double Hrv1Module::Mean(QList<double> intervalsDurations)
{
    double sum = 0;
    double mean;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        sum = sum + intervalsDurations.at(i);
    }
    mean = sum/intervalsDurations.size();
    return mean;
}

// standard deviation normal-to-normal [w m]
// odchylenie standardowe z dlugosci interwalow
double Hrv1Module::SDNN(QList<double> intervalsDurations)
{
    double mean = Mean(intervalsDurations);
    double squaresSum = 0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        squaresSum = squaresSum + (intervalsDurations.at(i) - mean)*(intervalsDurations.at(i) - mean);
    }
    double STNN = sqrt(1.0/(intervalsDurations.size())*squaresSum);
    return STNN;
}

// root mean square of successive differences w ms
// czyli pierwiastek sredniej kwadratow kolejnych intervalsow
double Hrv1Module::RMSSD(QList<double> intervalsDurations)
{
    double squaresSum = 0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        squaresSum = squaresSum + intervalsDurations.at(i)*intervalsDurations.at(i);
    }
    double RMSSD = sqrt(1.0/(intervalsDurations.size())*squaresSum);
    return RMSSD;
}

// liczba roznic pomiedzy interwalami, ktore sa wieksze niz 50 [ms]
int Hrv1Module::RR50(QList<double> intervalsDurations)
{
    int rr50 = 0;
    for(int i=0; i<intervalsDurations.size()-1; i++)
    {
        if(abs(intervalsDurations.at(i+1) - intervalsDurations.at(i)) > 50.0)
        {
            rr50++;
        }
    }
    return rr50;
}

// stosunek liczby roznic powyzej 50ms do wszystkich roznic [%]
double Hrv1Module::RR50Ratio(QList<double> intervalsDurations)
{
    int rr50 = RR50(intervalsDurations);
    int N = intervalsDurations.size();
    double percentage = rr50/(double)N;
    return percentage*100.0;
}

// odchylenie standardowe ze wszystkich średnich interwałów RR
//w 5 minutowych segmentach czasu całego zapisu [ms]
double Hrv1Module::SDANN(QList<double> intervalsDurations)
{
    QList<double> segmentsMeans;
    double tempSegmentTime = 0.0;
    double tempMean;
    QList<double> tempSegmentList;
    double fiveMinutes = 5.0*60.0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        if(tempSegmentTime < fiveMinutes)
        {
            tempSegmentList.append(intervalsDurations.at(i));
            tempSegmentTime += intervalsDurations.at(i);
        }
        else
        {
           tempMean = Mean(tempSegmentList);
           segmentsMeans.append(tempMean);
           tempSegmentTime = 0.0;
        }
    }
    double SDANN = SDNN(segmentsMeans);
    return SDANN;
}

// średnia z odchyleń standardowych interwałów RR w 5 minutowych segmentach czasu całego zapisu [ms]
double Hrv1Module::SDANNindex(QList<double> intervalsDurations)
{
    QList<double> segmentsSD;
    double tempSegmentTime = 0.0;
    double tempSD;
    QList<double> tempSegmentList;
    double fiveMinutes = 5.0*60.0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        if(tempSegmentTime < fiveMinutes)
        {
            tempSegmentList.append(intervalsDurations.at(i));
            tempSegmentTime += intervalsDurations.at(i);
        }
        else
        {
           tempSD = SDNN(tempSegmentList);
           segmentsSD.append(tempSD);
           tempSegmentTime = 0.0;
        }
    }
    double SDANNindex = Mean(segmentsSD);
    return SDANNindex;
}

// SDSD
// odchylenie standardowe z roznic pomiedzy kolejnymi interwalami
double Hrv1Module::SDSD(QList<double> intervalsDurations)
{
    double squaresSum = 0;
    for(int i=0; i<intervalsDurations.size()-1; i++)
    {
        squaresSum = squaresSum + (intervalsDurations.at(i+1) - intervalsDurations.at(i))*
                (intervalsDurations.at(i+1) - intervalsDurations.at(i));
    }
    double RMSSD = sqrt(1.0/(intervalsDurations.size()-1)*squaresSum);
    return RMSSD;
}

// funkcja zwracajaca dlugosci zalamkow [ms] na podstawie ich kolejnych czasow pojawiania sie
QList<double> Hrv1Module::getIntervalsDurations(QList<double> intervals, double frequency)
{
    double tempDuration;
    QList<double> intervalsDurations;
    for(int i=0; i<intervals.size()-1; i++)   //ilosc roznic jest mniejsza o jeden od wszystkich czasow zanotowania peakow
    {
        tempDuration = intervals.at(i+1)-intervals.at(i); // roznica czasowa miedzy kolejnymi interwalami
        tempDuration = tempDuration/frequency*1000;
        intervalsDurations.append(tempDuration);
    }
    return intervalsDurations;
}

// zwraca laczna dlugosc trwania wszystkich interwalow [minutes]
double Hrv1Module::timeOfAnalysis(QList<double> intervalsDurations)
{
    double timeOfAnalysis = 0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        timeOfAnalysis += intervalsDurations.at(i);
    }
    return timeOfAnalysis/1000/60;
}

/*
QList<double> Hrv1Module::DFT(QList<double> intervalsDurations)
{
    QList<double> amplitudes;
    double N = (double)intervalsDurations.size();
    std::complex<double> i(0.0, 1.0);
    double PI = std::atan(1)*4;
    for(double k = 0; k < N; k++)
    {
        std::complex<double> sum(0.0, 0.0);
        for(double n = 0; n < N; n++)
        {
            std::complex<double> exponent = std::exp((-i*k*n*2.0*PI)/N);
            sum += intervalsDurations.at(n)*exponent;
        }
        amplitudes.append(abs(sum));
    }
    return amplitudes;
}
*/

//FFT
QList<ComplexDouble> Hrv1Module::fft(QList<double> realSamples)
{
    int N = realSamples.size();
    fftw_complex *out = new fftw_complex[N];
    fftw_complex *in = new fftw_complex[N];
    fftw_plan plan_forward;

    for(int i=0;i<N;i++)
    {
        in[i][0] = realSamples[i]/1000.0;
        in[i][1] = 0.0;
    }
    plan_forward = fftw_plan_dft_1d(N,in,out,FFTW_FORWARD,FFTW_ESTIMATE);
    fftw_execute(plan_forward);
    QList<ComplexDouble> freqCoef;
    for(int i=0;i<N;i++) freqCoef.append(ComplexDouble(out[i][0],out[i][1]));
    delete[] out;
    delete[] in;
    fftw_destroy_plan(plan_forward);
    return freqCoef;
}

//zwraca moduly kwadratow poszczegolnych wspolczynnikow amplitudy
//zwraca kwadraty tylko N/2 +1 czestotliwosci gzdie N to wszystkie czestotliwosci
QList<double> Hrv1Module::getSquaredAmplitudes(QList<ComplexDouble> freqCoef)
{
    QList<double> squaredAmplitudes;
    for(int i=0; i<freqCoef.size(); i++)
    {
        squaredAmplitudes.append( abs(freqCoef.at(i))*abs(freqCoef.at(i)));//freqCoef.size());// /freqCoef.size());
    }
    return squaredAmplitudes;
}

//zwraca energie sygnalu pomiedzy dwoma czestotliwosciami
//czestotliwosci musza byc pomiedzy down_frequency i up_frequency <0.5 Hz
double Hrv1Module::getEnergyBetweenFreq(QList<double> squaredAmplitudes, double down_frequency, double up_frequency)
{
    double N = squaredAmplitudes.size();
    double energy = 0;
    double i = down_frequency*N;

    while(i<=N)
    {
        if(i/N > up_frequency)
            break;
        energy += squaredAmplitudes.at(i);
        i++;
        //qDebug() << energy;
    }
    return energy;
}
double Hrv1Module::getTP(QList<double> squaredAmplitudes)
{
    return getEnergyBetweenFreq(squaredAmplitudes, 0.0, 0.5);
}
double Hrv1Module::getHF(QList<double> squaredAmplitudes)
{
    return getEnergyBetweenFreq(squaredAmplitudes, 0.15, 0.5);
}
double Hrv1Module::getLF(QList<double> squaredAmplitudes)
{
    return getEnergyBetweenFreq(squaredAmplitudes, 0.04, 0.15);
}
double Hrv1Module::getVLF(QList<double> squaredAmplitudes)
{
    return getEnergyBetweenFreq(squaredAmplitudes, 0.003, 0.04);
}
double Hrv1Module::getULF(QList<double> squaredAmplitudes)
{
    return getEnergyBetweenFreq(squaredAmplitudes, 0.0, 0.003);
}
double Hrv1Module::getLFHF(QList<double> squaredAmplitudes)
{
    return getLF(squaredAmplitudes)/getHF(squaredAmplitudes);
}


// konwertuje QList<double> na char*  (czyli tablice charow)
char* Hrv1Module::charFromQList(QList<double> *a)
{
    int n = 0;
    char temp[100];
    for(int i = 0 ; i<a->size(); i++)
    {
        n += sprintf(temp, "%lf", a->at(i));
    }
    char *buffer = new char[n + a->size()-1 + 2 + 1];
    buffer[0] = '\0';
    strcat(buffer, "[");
    for(int i=0; i<a->size(); i++)
    {
        int p = sprintf(temp,"%lf", a->at(i));
        strcat(buffer, temp);
        strcat(buffer,",");
    }
    buffer[n+a->size() -1 +1] = ']';
    buffer[n+a->size() -1 +2] = '\0';
    return buffer;
}
//zwraca funkcje interpolujaca
spline1dinterpolant Hrv1Module::getInterpolant(QList<double>* dataX, QList<double>* dataY)
{
    char* charRRx = charFromQList(dataX);
    char* charRRy = charFromQList(dataY);
    real_1d_array x = charRRx;
    real_1d_array y = charRRy;
    spline1dinterpolant interpolanta;
    spline1dbuildcubic(x, y, interpolanta);
    double samplingPeriod = getSamplingPeriod(dataX);
    return interpolanta;
}
QList<double> Hrv1Module::getYSamplesForFF(QList<double> equidistantNodes, spline1dinterpolant interpolanta)
{
    //spline1dinterpolant s = getInterpolant(RRx, RRy);
    //double v;
    //double samplingPeriod = getSamplingPeriod(RRx);

    QList<double> FFsamplesY;
    for(double i=0; i<equidistantNodes.size(); i++)
    {
        FFsamplesY.append( (double)(spline1dcalc(interpolanta, equidistantNodes.at(i)) ) );
    }

    return FFsamplesY;
}

//funkcja zwracajaca wartosci funkcji interpolujacej w pkt QListy equidistantNodes
QList<double> Hrv1Module::getValuesOfInterpolantOnEquidistantNodes(QList<double> equidistantNodes,
                                                                   spline1dinterpolant interpolanta)
{
    QList<double> ySamples;
    int numberOfSamples = equidistantNodes.size();
    for(double i=0; i<numberOfSamples; i++)
    {
        ySamples.append( (double)(spline1dcalc(interpolanta, equidistantNodes.at(i)) ) );
    }
    return ySamples;
}

//funkcja zwracajaca rownopoodzielane pkt  (docelowo potrzebne na osi X)
//zwraca tyle pkt ile jest w RR_x oddalone od siebie o  (czas wystapienia ostatnio x w RR_x)/coefficient
QList<double> Hrv1Module::getEquidistantNodes(QList<double> * RR_x, double coefficient)
{
    QList<double> equidistantNodes;
    double samplingPeriod = getSamplingPeriod(RR_x)/coefficient;
    int i = 0;
    int NumberOfNodes = RR_x->size()*coefficient;
    for(i = 0; i<NumberOfNodes; i++)
    {
        equidistantNodes.append(i*samplingPeriod);
    }
    return equidistantNodes;
}

//zwraca czas probkowania w FFT
double Hrv1Module::getSamplingPeriod(QList<double>* RRx)
{
    return RRx->at(RRx->size()-1)/(RRx->size()-1)/1.0;
}



void Hrv1Module::Process(EcgData *data)
{
#ifdef DEVELOP
    qDebug() << "Hrv1Module";
#endif

    QList<double> samplesNrOfPeaks;
    int intervalsSize = data->r_peaks->size();
    for(int i=0; i<intervalsSize; i++)
    {
        samplesNrOfPeaks.append(data->r_peaks->at(i));  //czasy kolejnych interwalow
    }

    QList<double> intervalsDurations = getIntervalsDurations(samplesNrOfPeaks, (double)data->info->frequencyValue);
    QList<double> wartosci;

    //STATISTICAL METHODS
    data->Mean = Mean(intervalsDurations);
    data->SDNN = SDNN(intervalsDurations);
    data->RMSSD = RMSSD(intervalsDurations);
    data->RR50 = RR50(intervalsDurations);
    data->RR50Ratio = RR50Ratio(intervalsDurations);
    data->SDANN = SDANN(intervalsDurations);
    data->SDANNindex = SDANNindex(intervalsDurations);
    data->SDSD = SDSD(intervalsDurations);

    //FREQUENCY METHODS
    //wyrysowywanie RR
    QList<double> *temp_RR_x = new QList<double>();
    QList<double> *temp_RR_y = new QList<double>();
    double temp_x=0;
    for(int i=0; i<intervalsDurations.size(); i++)
    {
        temp_RR_x->append(temp_x);
        temp_x += intervalsDurations.at(i)/1000;
        temp_RR_y->append(intervalsDurations.at(i));
    }
    data->RR_x = temp_RR_x;
    data->RR_y = temp_RR_y;

    // Wyliczenie wspolrzednych X i Y dla probek z funkcji interpolacji, ktore potem sa potrzebne do FFT

    QList<double> FFsamplesY;              // probki (wartosci y) dla fft
    QList<double> FFsamplesX;
    QList<double> interpolantX;
    QList<double> interpolantY;

    //zinterpolowanie funkcji na podstawie RR
    spline1dinterpolant interpolanta = getInterpolant(temp_RR_x, temp_RR_y);

    //otrzymanie probek dla fouriera wyciagnietych z funkcji interpolowanej
    FFsamplesX = getEquidistantNodes(temp_RR_x, 1.0);
    FFsamplesY = getYSamplesForFF(FFsamplesX, interpolanta);
    interpolantX = getEquidistantNodes(temp_RR_x, 80.0);
    interpolantY = getValuesOfInterpolantOnEquidistantNodes(interpolantX, interpolanta);

    data->fftSamplesX = FFsamplesX;
    data->fftSamplesY = FFsamplesY;
    data->interpolantX = interpolantX;
    data->interpolantY = interpolantY;

    //wyrysowywanie Fouriera
    QList<ComplexDouble> freqCoef = fft(FFsamplesY);
    QList<double> squaredAmplitudes = getSquaredAmplitudes(freqCoef);   // polowa amplitud
    QList<double> *temp_fft_x = new QList<double>();
    QList<double> *temp_fft_y = new QList<double>();

    int posFreqNumber = (freqCoef.size()+1)/2;
    for(int i=0; i<posFreqNumber; i++)
    {
        temp_fft_x->append(((double)i)/freqCoef.size());
        temp_fft_y->append(squaredAmplitudes.at(i));
    }
    data->fft_x = temp_fft_x;
    data->fft_y = temp_fft_y;

    //Frequency Coefficients
    data->TP = getTP(squaredAmplitudes);
    data->HF = getHF(squaredAmplitudes);
    data->LF = getLF(squaredAmplitudes);
    data->VLF = getVLF(squaredAmplitudes);
    data->ULF = getULF(squaredAmplitudes);
    data->LFHF = getLFHF(squaredAmplitudes);
}
