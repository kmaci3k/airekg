#ifndef HRV1MODULE_H
#define HRV1MODULE_H

#include<fftw3.h>
#include<complex>

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "interpolation.h"

using namespace alglib;

//#include "QList";  // moje

#include "Common/processingmodule.h"


typedef std::complex<double> ComplexDouble;
typedef std::vector<double> VectorDouble;
//typedef std::vector<unsigned int> VectorUnsignedInt;
typedef std::vector<ComplexDouble> VectorComplexDouble;
//typedef std::pair<double, unsigned int> PairDoubleUnsignedInt;
//typedef std::vector<PairDoubleUnsignedInt> VectorPairDoubleUnsignedInt;

class Hrv1Module : public ProcessingModule
{
public:
    Hrv1Module();
    virtual void Process(EcgData *data);

    //STATISTICAL ANALYSIS*************************************************************************8
    double Mean(QList<double> IntervalsDurations);
    double SDNN(QList<double> intervalsDurations);
    double RMSSD(QList<double> intervalsDurations);
    int RR50(QList<double> intervalsDurations);
    double RR50Ratio(QList<double> intervalsDurations);
    double SDANN(QList<double> intervalsDurations);
    double SDANNindex(QList<double> intervalsDurations);
    double SDSD(QList<double> intervalsDurations);

    QList<double> getIntervalsDurations(QList<double> Intervals, double frequency);
    double timeOfAnalysis(QList<double> intervalsDurations);

    //FREQUENCY ANALYSIS*************************************************************************8

    // Interpolation
    spline1dinterpolant interpolate(QList<double> *RR_x, QList<double> *RR_y);
    spline1dinterpolant getInterpolant(QList<double>* dataX, QList<double>* dataY);
    QList<double> getEquidistantNodes(QList<double> *RR_x, double coefficient);
    QList<double> getValuesOfInterpolantOnEquidistantNodes(QList<double> equidistantNodes,
                                                                       spline1dinterpolant interpolanta);

    char* charFromQList(QList<double> *a);
    double getSamplingPeriod(QList<double>* RRx);

    // tworzenie fft
    QList<double> getYSamplesForFF(QList<double> equidistantNodes, spline1dinterpolant interpolanta);
    QList<ComplexDouble> fft(QList<double> realSamples);

    QList<double> getSquaredAmplitudes(QList<ComplexDouble> freqCoef);

    // metody do liczenia mocy danych czestotliwosci
    double getEnergyBetweenFreq(QList<double> squaredAmplitudes, double down_frequency, double up_frequency);
    double getTP(QList<double> squaredAmplitudes);
    double getHF(QList<double> squaredAmplitudes);
    double getLF(QList<double> squaredAmplitudes);
    double getVLF(QList<double> squaredAmplitudes);
    double getULF(QList<double> squaredAmplitudes);
    double getLFHF(QList<double> squaredAmplitudes);
};

#endif // HRV1MODULE_H
