#include "appcontroller.h"
#include "Common/ecgdata.h"
#include "Common/ecgentry.h"

#include "ECG_BASELINE/ecg_baselinemodule.h"
#include "R_PEAKS/r_peaksmodule.h"
#include "WAVES/wavesmodule.h"
#include "HRV1/hrv1module.h"
#include "HRV2/hrv2module.h"
#include "HRV_DFA/hrv_dfamodule.h"
#include "QRS_CLASS/qrs_classmodule.h"
#include "ST_INTERVAL/st_intervalmodule.h"
#include "T_WAVE_ALT/t_wave_altmodule.h"
#include "HRT/hrtmodule.h"

#include "Common/supervisorymodule.h"

#include <QThread>

AppController::AppController(QObject *parent) : QObject(parent)
{
    this->InitializeDependencies();
}

void AppController::InitializeDependencies()
{
    Ecg_BaselineModule *ecg_BaselineModule = new Ecg_BaselineModule();
    R_PeaksModule *r_PeaksModule = new R_PeaksModule();
    WavesModule *wavesModule = new WavesModule();
    Hrv1Module *hrv1Module = new Hrv1Module();
    Hrv2Module *hrv2Module = new Hrv2Module();
    Hrv_DfaModule *hrv_DfaModule = new Hrv_DfaModule();
    Qrs_ClassModule *qrs_ClassModule = new Qrs_ClassModule();
    St_IntervalModule *st_IntervalModule = new St_IntervalModule();
    T_Wave_AltModule *t_Wave_AltModule = new T_Wave_AltModule();
    HrtModule *hrtModule = new HrtModule();

    ecg_BaselineModule->SetNextChild(r_PeaksModule);
    ecg_BaselineModule->SetNextChild(wavesModule);
    ecg_BaselineModule->SetNextChild(qrs_ClassModule);
    ecg_BaselineModule->SetNextChild(t_Wave_AltModule);
    ecg_BaselineModule->SetNextChild(st_IntervalModule);

    r_PeaksModule->SetNextChild(wavesModule);
    r_PeaksModule->SetNextChild(hrv1Module);
    r_PeaksModule->SetNextChild(hrv2Module);
    r_PeaksModule->SetNextChild(hrv_DfaModule);
    r_PeaksModule->SetNextChild(hrtModule);

    wavesModule->SetNextChild(qrs_ClassModule);
    wavesModule->SetNextChild(st_IntervalModule);
    wavesModule->SetNextChild(t_Wave_AltModule);
    wavesModule->SetNextChild(hrtModule);

    qrs_ClassModule->SetNextChild(hrtModule);

    this->supervisor = new SupervisoryModule("ECG_BASELINE", ecg_BaselineModule);
    this->supervisor->AppendModule("R_PEAKS", r_PeaksModule);
    this->supervisor->AppendModule("WAVES", wavesModule);
    this->supervisor->AppendModule("HRV1", hrv1Module);
    this->supervisor->AppendModule("HRV2", hrv2Module);
    this->supervisor->AppendModule("HRV_DFA", hrv_DfaModule);
    this->supervisor->AppendModule("HRT", hrtModule);
    this->supervisor->AppendModule("QRS_CLASS", qrs_ClassModule);
    this->supervisor->AppendModule("ST_INTERVAL", st_IntervalModule);
    this->supervisor->AppendModule("T_WAVE_ALT", t_Wave_AltModule);

}

void AppController::BindView(AirEcgMain *view)
{
    this->connect(view, SIGNAL(loadEntity(QString,QString)), this, SLOT(loadData(QString,QString)));
    this->connect(view, SIGNAL(switchSignal(int)), this, SLOT(switchSignal(int)));
    this->connect(this, SIGNAL(patientData(EcgData*)), view, SLOT(receivePatientData(EcgData*)));
    this->connect(view, SIGNAL(switchEcgBaseline(int)), this, SLOT(switchEcgBaseline(int)));
    this->connect(view, SIGNAL(switchRPeaks(unsigned char)), this, SLOT(switchRPeaks(unsigned char)));
    this->connect(view, SIGNAL(switchTWA(unsigned char)), this, SLOT(switchTWA(unsigned char)));
    this->connect(view, SIGNAL(run()), this, SLOT(run()));
    this->connect(this, SIGNAL(processingResults(EcgData*)), view, SLOT(receiveResults(EcgData*)));
    this->connect(view, SIGNAL(qrsClassChanged(int,int)),this,SLOT(sendQRSData(int,int)));
    this->connect(this,SIGNAL(sendQRSData(QRSClass,int)),view,SLOT(receiveQRSData(QRSClass,int)));
    this->connect(view, SIGNAL(runSingle(QString)), this, SLOT(runSingle(QString)));
    this->connect(this, SIGNAL(singleProcessingResult(bool, EcgData*)), view, SLOT(receiveSingleProcessingResult(bool, EcgData*)));
    this->connect(view, SIGNAL(qrsClustererChanged(ClustererType)),this,SLOT(qrsClustererChanged(ClustererType)));
    this->connect(view, SIGNAL(qrsGMaxClustersChanged(int)),this,SLOT(qrsGMaxClustersChanged(int)));
    this->connect(view, SIGNAL(qrsGMaxKIterations(int)),this,SLOT(qrsGMaxKIterations(int)));
    this->connect(view, SIGNAL(qrsGMinClustersChanged(int)),this,SLOT(qrsGMinClustersChanged(int)));
    this->connect(view, SIGNAL(qrsKClustersNumberChanged(int)),this,SLOT(qrsKClustersNumberChanged(int)));
    this->connect(view, SIGNAL(qrsMaxIterationsChanged(int)),this,SLOT(qrsMaxIterationsChanged(int)));
    this->connect(view, SIGNAL(qrsParallelExecutionChanged(bool)),this,SLOT(qrsParallelExecutionChanged(bool)));

}

void AppController::loadData(const QString &directory, const QString &name)
{
    EcgEntry *entry = new EcgEntry();
    QString *response = new QString("");
    if(!entry->Open(directory, name, *response))
    {
        return;
    }
    if(this->entity)
        this->supervisor->ResetModules();
    this->entity = entry->entity;

    emit patientData(this->entity);
}

void AppController::switchEcgBaseline(int type)
{
    if(this->entity)
        this->entity->settings->EcgBaselineMode = type;
}

void AppController::switchRPeaks(unsigned char type)
{
    if(this->entity)
        this->entity->settings->RPeaksMode = type;
}

void AppController::sendQRSData(int index, int type)
{
    if (!this->entity || !this->entity->classes || !this->entity->waves || !this->entity->ecg_baselined || index < 0)
        return;
    QRSClass qrsSegment;

    if (type == 1)
    {
        if (this->entity->classes->count() > index)
        {
            qrsSegment = this->entity->classes->at(index);
        }
        else
            return;
    }
    else
    {
        if (this->entity->waves->count() <= index)
            return;
        int begin = this->entity->waves->at(index)->QRS_onset;
        int end = this->entity->waves->at(index)->QRS_end + 1;

        qrsSegment.representative = new QVector<double>();
        for(int i = begin; i < end; i++)
            qrsSegment.representative->append(this->entity->ecg_baselined->at(i));
    }
    emit sendQRSData(qrsSegment,type);
}

void AppController::switchTWA(unsigned char type)
{
    if(this->entity)
        this->entity->TWA_mode = type;
}

void AppController::qrsClustererChanged(ClustererType type)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.clusterer = type;
}

void AppController::qrsMaxIterationsChanged(int maxIters)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.maxIterations = maxIters;
}

void AppController::qrsParallelExecutionChanged(bool flag)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.parallelExecution = flag;
}

void AppController::qrsGMinClustersChanged(int minClusters)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.minClusterNo = minClusters;
}

void AppController::qrsGMaxClustersChanged(int maxClusters)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.maxClusterNo = maxClusters;
}

void AppController::qrsGMaxKIterations(int maxIters)
{
    if(this->entity)
        this->entity->settings->qrsClassSettings.insideIterations = maxIters;
}

void AppController::qrsKClustersNumberChanged(int noClusters)
{
    if(this->entity)
    {
        this->entity->settings->qrsClassSettings.minClusterNo = noClusters;
        this->entity->settings->qrsClassSettings.maxClusterNo = noClusters;
    }
}

void AppController::run()
{
    if(this->entity)
    {
        this->supervisor->entity = this->entity;
        connect(this->supervisor, SIGNAL(Finished()), this, SLOT(onThreadFinished()));
        this->supervisor->start();
    }
}

void AppController::onThreadFinished()
{
    emit this->processingResults(this->entity);
}

void AppController::runSingle(QString hash)
{
    if(this->entity)
    {
        bool processed = this->supervisor->RunSingle(this->entity, hash);
        emit singleProcessingResult(processed, this->entity);
    }
}

void AppController::switchSignal(int index)
{
    this->entity->settings->signalIndex = index;
    this->supervisor->ResetModules();
}
